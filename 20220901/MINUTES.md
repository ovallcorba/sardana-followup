# Minutes for the Sardana Follow-up Meeting - 2022/09/01

Held on Thursday 2022/09/01 at 14:00

Present:
- Zibi (ALBA)
- Michal (SOLARIS)
- Teresa (DESY)
- Johan (MAXIV) chair

## Agenda

1. Sardana Configuration Tool - see [!1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
   
   - current status and how to proceed?
     - Scheduled meeting September 5th, 14:00 to discuss next steps.
     - Zibi made a sar_demo example and had several comments:
       + tango_host could be different for different pools, but global default may be useful.
       + global "name" not used/needed?
       + using separate YAML documents instead of pool/macroserver toplevels?
       + Axis "parameters" -> "attributes"
       + element attributes either simple value, or a dict of value, abs_change etc. More concise.
       + Instrument class is not a python class name, but typically a nexus class.
       + use pattern to build tango/full name from type/name/axis?
       + allow splitting the config in several files?

2. Discussion about Bug Squashing Party #2  
  
   - dates, most probably:
     - from 25.10 (Tuesday) afternoon or eventually 26.10 (Wednesday) morning
     - to 27.10 (Thursday) afternoon/evening or eventually 28.10 (Friday) afternoon/evening
     - Decision: Tuesday EM - Thursday (possibly remote also Friday)
   - location: ALBA (Barcelona) and "hybrid" for remote participants
   - participants:
     - SOLARIS (Michal and colleagues)
     - Teresa (DESY) and Daniel (MBI) said they will try it
     - what about MAXIV? Johan will check.
     - ALBA (Zibi and colleagues)
   - Preparation works
     - review list of issues, label them (category, size, impact) - use questionnaire results
     - review Kanban board
     - allow remote participation?
     - send announcement
     - organize prior review of Sardana core and preparation of environment e.g. during next follow-up meeting?
     
   - Introduction to explain the sardana layers (server/client...)
  
   - Scheduled meeting October 10th, 14:00 to go through sardana tickets for labelling.
  
3. Urgent user problems/issues - Round table
   
    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
      - Again facing problems with taurus unsubscribing in attribute's `__del__()` - https://gitlab.com/tango-controls/pytango/-/issues/413. Found a bug in PyTango/cppTango - to be reported. Can be reproduced with `test_dev_gc` macro - https://gitlab.com/taurus-org/taurus/-/merge_requests/1093#note_531346632. We should add this macro to the testsuite.
      - Hangs on Pool shutdown if there are event subscriptions present
      - expconf wrongly modify old PreScanSnapshot names - [#1771](https://gitlab.com/sardana-org/sardana/-/issues/1771). Probably could be closed as *wontfix*.
      
    - MBI
    - ...

4. Review pending points from the previous meetings

   - From last meeting:
     - [ ] Johan and Teresa were going to try sardana-jupyter. Any feedback?
       - Not really, no time yet.
     - [X] SEP20: The configuration file example will be put in the SEP20 branch (or dedicated repo) to collect comments.
     - [X] Sardana release jul22 is done, and ALBA has already tried it. 
        
   - From previous meetings:
     - Discussion on limit switches
       - [X] issue raised about macro names for setting limits unification [#1641](https://gitlab.com/sardana-org/sardana/-/issues/1641) -  Daniel will propose a MR for the naming
           - Work under way in MR 1790 (dshick)
     - [x] Reported by MAXIV: Issue with extra axis attributes on pseudo motor not causing change events on position
           https://gitlab.com/sardana-org/sardana/issues/1693 <- related
           - no update x 2
  
5. Overview of current developments / MR distribution
   - Draft: Add fixtures for pool core tests and controller plugin tests [!1474](https://gitlab.com/sardana-org/sardana/-/merge_requests/1474)
     - We try to get this done for the bug squashing party.
   - scanstats for 2d/3d scans [#1748](https://gitlab.com/sardana-org/sardana/-/issues/1748)
   - call newfile macro in expconf storage dialog [#1766](https://gitlab.com/sardana-org/sardana/-/issues/1766)
   - Simplify PseudoMotor configuration? [#1702](https://gitlab.com/sardana-org/sardana/-/issues/1702)
   - Wrong PseudoMotor first movement after externally changing physical motor
   - REVIEW
[#1502](https://gitlab.com/sardana-org/sardana/-/issues/1502)
   - Draft: Add error catching for ending records [!1770](https://gitlab.com/sardana-org/sardana/-/merge_requests/1770)
     - Needs more communication!

6. AOB
   - Next meeting?
   - October 13th organized by SOLARIS (Michal)
