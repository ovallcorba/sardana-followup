# Minutes for the Sardana Follow-up Meeting - 2024/01/11

Held on Thursday 2024/01/11 at 14:00

Organizer: Johan @ MAX IV

Participants: Johan, Zibi, Marti, Oriol, Michal, Teresa

## Minutes

1. Sardana Redis recorder using blissdata 1.0.
   - Comments after the meetings with ESRF?
     - DESY interested in "flint", a visualization tool for scan data, 1D, 2D, ... Flint is part of Bliss but will be decoupled.
       Flint can use either Redis or fall back to written files.
     - ALBA interested in nexus-writer, because ALBA is moving acquisition to nexus format. This relies on Redis.
   - Status?
     - New meeting in March to synch and decide future direction.

2. Sardana configuration improvements (SEP20).
   - bug fixes etc
     - https://gitlab.com/sardana-org/sardana/-/merge_requests/1972 (detect settings)
     - https://gitlab.com/sardana-org/sardana/-/merge_requests/1943 (door names)
     - https://gitlab.com/sardana-org/sardana/-/merge_requests/1916 (tango host)
     - https://gitlab.com/sardana-org/sardana/-/merge_requests/1971 (old meas grp format)
   - configuration spread in multiple files
     - MR https://gitlab.com/sardana-org/sardana/-/merge_requests/1968 in need of review
   - anything else?
     - https://gitlab.com/sardana-org/sardana/-/issues/1916 (adding "description")

3. Operation highlights, user's feedback, issues, etc. - Round table
   - ALBA
     - Need for pseudomotor based on 3 physical motors, where sometimes only 2 motors should be moved. Some investigation needed for whether it can be supported already.
     - Hotfix for meshct scan proposed, to solve issue where the non-continuous motor is being included in the accelleration calculations.
   - DESY
   - MAX IV
     - Observations around https://gitlab.com/sardana-org/sardana/-/issues/113 (pool/ms startup woes)
   - MBI Berlin
   - SOLARIS
     - Starting to use showscan at beamlines, to replace an old internal GUI

4. Review pending points from the previous meetings

   - From last meeting
     - [ ] Sardana roadmap: set milestone for the next release and assign short terms issues to it + add assignees
       - Zibi will take care of setting this up
     - [x] missing aliases of elements (Pool, MacroServer, Door...) crashes configuration dump - Michal will create an issue
   - From previous meetings:
     - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab (no update x2)

5. Jan24 release status
   - select MR/issues for the milestone
     - issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues
     - MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests
   - https://gitlab.com/sardana-org/sardana/-/merge_requests/1970 is important for the release
   - https://gitlab.com/sardana-org/sardana/-/merge_requests/1957 could be nice? New contributor.
   - https://gitlab.com/sardana-org/sardana/-/issues/1924 renaming elements. Consider it, unless it becomes complex.

6. Overview of current developments / MR distribution

7. Review of the oldest 15 issues in the backlog
   - Closed https://gitlab.com/sardana-org/sardana/-/issues/110
   - Closed https://gitlab.com/sardana-org/sardana/-/issues/112
   - Updated https://gitlab.com/sardana-org/sardana/-/issues/130 (last)

8. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.
     - Needs discussion of pros and cons.
       - Affects CI/CD pipeline minutes, Sardana is currently OK with its number of minutes, Tango is short on it
       - Would eliminate some paper work (renewal)
       - Some benefit from being under the Tango namespace, e.g. being easier to find (maybe?)
       - Should consider also what happens to Taurus (which is maybe less dependent on Tango)
       - Also some amount of work to make the move
   - From ALBA:
     - Marti is leaving for another job
     - Zibi is taking over as lead of the "controls and data acquisition" team at ALBA which means he will have to take on other duties as well. ALBA will consider how to redistribute the load.
   - Next meeting?
     - Organized by DESY/Teresa
     - Date 2024-02-08 14:00
     - I got a suggestion from Vincent that we could announce the meetings at https://indico.tango-controls.org/

