# Agenda for the Sardana Follow-up Meeting - 2024/01/11

Held on Thursday 2024/01/11 at 14:00

Organizer: Johan @ MAX IV

## Agenda

1. Sardana Redis recorder using blissdata 1.0.
   - Comments after the meetings with ESRF?
   - Status?

2. Sardana configuration improvements (SEP20).
   - bug fixes etc
     - https://gitlab.com/sardana-org/sardana/-/merge_requests/1972 (detect settings)
     - https://gitlab.com/sardana-org/sardana/-/merge_requests/1943 (door names)
     - https://gitlab.com/sardana-org/sardana/-/merge_requests/1916 (tango host)
     - https://gitlab.com/sardana-org/sardana/-/merge_requests/1971 (old meas grp format)
   - configuration spread in multiple files
     - MR https://gitlab.com/sardana-org/sardana/-/merge_requests/1968 in need of review
   - anything else?

3. Operation highlights, user's feedback, issues, etc. - Round table
   - ALBA
   - DESY
   - MAXIV
     - Observations around https://gitlab.com/sardana-org/sardana/-/issues/113 (pool/ms startup woes)
   - MBI Berlin
   - SOLARIS

4. Review pending points from the previous meetings

   - From last meeting
     - [ ] Sardana roadmap: set milestone for the next release and assign short terms issues to it + add assignees
     - [ ] missing aliases of elements (Pool, MacroServer, Door...) crashes configuration dump - Michal will create an issue
   - From previous meetings:
     - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab (no update x1)

5. Jan24 release status
   - select MR/issues for the milestone
     - issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues
     - MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests

6. Overview of current developments / MR distribution

7. Review of the oldest 15 issues in the backlog

8. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.
     - Needs discussion of pros and cons.
   - Next meeting?
