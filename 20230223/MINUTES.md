# Minutes from the Sardana Follow-up Meeting - 2023/02/23

Held on Thursday 2023/02/23 at 14:00

Organizer: Johan @ MAX IV

## Participants
    Johan, Zibi, Theresa, Michal, Jordi (ALBA)

## Agenda

1. Operation highlights, user's feedback, issues, etc. - Round table
   
    - SOLARIS
      - In one GUI there is a big "Panic" button that should stop everything, probably by invoking a Pool Stop command. If a controller is broken for some reason, this command may not work.
        Zibi: there should be a related fix in Sardana 3.3.3 (jul22), after a report from MAX IV. With this fix, offline controllers will be skipped, but it may be that it can still fail?
        Michal: beamline is probably on 3.1. Will try this out.
      - Question: is it possible to *write* controller properties from within the controller code?
        Zibi: controller properties are read only, loaded on startup.
    - MAXIV
    - DESY
      - Problems with aborting macros with Ctrl-C, seemingly caused by Taurus GUIs subscribing to elements. At least, the problems went away when some GUI was killed. The GUI was in particular polling the Door state.
        Potentially related issue https://gitlab.com/sardana-org/sardana/-/issues/1431
        Sardana logs provided by Teresa.
    - ALBA
      - Migrated all BLs to sardana 3 previously, now also all ID systems are on 3. Only optics lab still on 2.
      - Problem with hangs on moves were reproduced on one BL, running with Tango 7 (!). This was related to GC in taurus, leading to a deadlock. Solution may be to replace Taurus with a normal proxy, or just upgrade Tango. Further investigation needed.
    - MBI
    - ...

2. Review pending points from the previous meetings

  - From last meeting:
    - [ ] Zibi will provide diagrams on what controllers should return in `ReadOne()` - requested by MAXIV
       - No news yet
    - [ ] Wojciech will answer the comments in th MR (Pool should not log an ERROR if hkl is not installed - #394)
       - He promised to look at this MR tomorrow.
  - From previous meetings:
     - PseudoCounter in flyscan
       - [ ] Zibi will at least comment of what exactly is needed
          No update
     - Hangs on Pool shutdown if there are event subscriptions present 
       - [x] Johan will try to reproduce it like this. It should be then reported to Tango.
          This is discussed in #1832, and traced back to Taurus.
          Johan will make a Tango issue asking if we still need the cleanup in Taurus for recent tango versions.
	
3. Overview of current developments / MR distribution
   - SEP20 report
   - Review project board
      - A few MRs left to be merged for the jan release, no blockers
      - Several from ALBA mostly regarding macros, should be reviewed and possibly merged
      
4. Jan23 Release
   - testing platforms (see minutes from the previous meeting)
     - Same conclusion: Manual tests will be done in: conda (with python 3.10) - Johan, debian11 (sardana3, python3) - Teresa,
       centos7 - Michal, debian9 or/and debian10, Windows - Zibi.
     - Agreed to do manual tests for the SEP20 config script which requires changing the setting for numerical ids
     
5. Review of the oldest 15 issues in the backlog
   

6. AOB
  - Taurus "hybrid" workshop at ESRF in march, ALBA will participate
  - Solaris is planning a Sardana workshop, also "hybrid", probably for June, and tentative topic is continuous scans. 
  - Next meeting on 2023-03-30, arranged by Michal/Solaris.
