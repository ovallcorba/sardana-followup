# Agenda for the Sardana Follow-up Meeting - 2023/02/23

To be held on Thursday 2023/02/23 at 14:00

## Agenda

1. Operation highlights, user's feedback, issues, etc. - Round table
   
    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - MBI
    - ...

2. Review pending points from the previous meetings

  - From last meeting:
    - [ ] Zibi will provide diagrams on what controllers should return in `ReadOne()` - requested by MAXIV
    - [ ] Wojciech will answer the comments in th MR (Pool should not log an ERROR if hkl is not installed - #394)
  - From previous meetings:
     - PseudoCounter in flyscan
       - [ ] Zibi will at least comment of what exactly is needed
     - Hangs on Pool shutdown if there are event subscriptions present
       - [ ] Johan will try to reproduce it like this. It should be then reported to Tango.
	
     
3. Overview of current developments / MR distribution
   - SEP20 report
   - Review project board
      
4. Jan23 Release
   - testing platforms (see minutes from the previous meeting)
      
5. Review of the oldest 15 issues in the backlog

6. AOB
  - Next meeting?
