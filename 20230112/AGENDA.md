# Agenda for the Sardana Follow-up Meeting - 2023/01/12

To be held on Thursday 2023/01/12 at 14:00

## Agenda
  
1. Operation highlights, user's feedback, issues, etc. - Round table
   
    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - MBI
    - ...

2. Review pending points from the previous meetings

   - From last meeting:
     - Pool should not log an ERROR if hkl is not installed - #394
       - [ ] Michal will ping Wojciech       
     - scanstats for 2d/3d scans
       - [ ] Teresa will make the final review
     - spock takes too much time to write data 
       - [ ] we will try to reproduce it at ALBA
     - Change the default behaviour of expconf view to the simple one
       - [ ] Zibi will ping Jordi
     - call newfile macro in expconf storage dialog
       - We ping S2Innovation
     - PseudoCounter in flyscan
       - [ ] Zibi will at least comment of what exactly is needed

   - From previous meetings:
       - [ ] Hangs on Pool shutdown if there are event subscriptions present
         - no update x3, no issue yet
          
3. Overview of current developments / MR distribution
   - SEP20 works
      - [#1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
   - Review project board

4. Jan23 release

5. Review of the oldest 15 issues in the backlog

6. AOB
   - Next meeting?
