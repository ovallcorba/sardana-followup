# Minutes from  the Sardana Follow-up Meeting - 2023/01/12

Held on Thursday 2023/01/12 at 14:00


## Participants:
    Johan, Michal, Teresa, Zibi

## Minutes
  
1. Operation highlights, user's feedback, issues, etc. - Round table
   
    - SOLARIS
       Not managed to update Sardana and Taurus during current shutdown,
       probably in the sommer ones. User operation starts next week. Tango updated to 3.5 
    - MAXIV
       In shutdown, no particular issues.
       Confusions about which kind of data should be return by controllers.
       Main issue what to return if no data is there during a scan.
       Zibi pointed out to a documentation about it:
       https://sardana-controls.org/devel/howto_controllers/howto_countertimercontroller.html#get-counter-value
       but it is not complete. He will do some diagram about it.
    - DESY
       In Shutdown.
    - ALBA
       As it was said in the previous meeting, all beamlines were migrated to sardana/python 3,
       but they still have some other systems (injection devices, ...) were sardana is used and it
       was not migrated. In the current shutdown they are migrating those systems to sardana/python 3.
       In this migration they are losing some memorized attributes, and do not understand the reason. 
       They found moves that get stack. In 2 beamlines seldom, but in a third one it happles more
       often and can be reproduced it. They got the backtrace and it get stack when executing garbage
       collection from taurus and some events and threads (also from Tango) are involved. Zibi could
       not reproduce it in his PC. He thinks that it is a Tango issue but due to the Taurus way of
       unsubscribe to events. They continue investigating.

2. Review pending points from the previous meetings

   - From last meeting:
     - Pool should not log an ERROR if hkl is not installed - #394
       - [x] Michal will ping Wojciech
        Wojciech will answer the comments in th MR
     - scanstats for 2d/3d scans
       - [x] Teresa will make the final review
         MR is merged
     - spock takes too much time to write data 
       - [x] we will try to reproduce it at ALBA
        The problem was the nfs mounting, they change it to async and the problem is
	gone. Zibi will send an e-mail for checking how the mounting of nfs
	is done in the other institutes.
	There is a MR #1827 from Adriana (Solaris) and Roberto (Alba). The MR is not
	needed becaues the origin of the problem is different. We close it for now,
	since it makes the code more complicated without need.
     - Change the default behaviour of expconf view to the simple one
       - [x] Zibi will ping Jordi
       Jordi and Michal are working on that. It has to be decided if the simple
       view should have output enable or disable and the plotting. Roberto will be
       asked about that, since he works with some users using often the output.
     - call newfile macro in expconf storage dialog
       - [x] We ping S2Innovation
       They will not continue with this. They contacted Daniel and there is some
       work going on (in project board in "to dev")
     - PseudoCounter in flyscan
       - [ ] Zibi will at least comment of what exactly is needed
       Nothing done

   - From previous meetings:
       - [x] Hangs on Pool shutdown if there are event subscriptions present
	 One has to try to reproduce it without sardana, with a simple
	 device server, making a subscription and not unsubscribe when killing
	 the server.
	 Johan will try to reproduce it like this. It should be then
	 reported to Tango.
          
3. Overview of current developments / MR distribution
   - SEP20 works
      - [#1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
      Johan was working on that. The whole functionality is in place, they are testing
      it. What remains is a kind of user experience on that. They will try to give
      it for being use and make documentation. Also look at some work flows like using it
      from spock. The proof of concept state is however done.
      Alba did some tests and some comments were done. Issue #1821 discusses the roles of pseudo
      motors, there is a discussion there how it should be implemented. This discussion will be
      outside the SEP20. The MR from Zibi related to this will be close. Numerical IDs will not
      be used but the alias, which are also unique.
      Johan will reorganize the code (set it in the config directory), and will add some
      README files before implementing the documentation.
      Zibi will work on the Sardana CLI and Johan will take over for the configuration part.
      The CLI will be a different MR.
      
   - Review project board
      Postponed for lack of time.

4. Jan23 Release
     It will be done by MaXIV (Johan) and Alba (Zibi)
     SEP20 and Command Line Config Tool should be included there.
     MRs reviewed and some assigned to Jan23 Release.
     Manual tests will be done in: conda (with python 3.10) - Johan, debian11 (sardana3, python3) - Teresa,
     centos7 - Michal, debian9 or/and debian10, Windows - Zibi.
     On 23rd January the scope will be closed.

5. Review of the oldest 15 issues in the backlog
     Postponed for lack of time.

6. AOB
   - Next meeting 23rd February 14:00 organized by MaXIV (Johan)
