# Minutes for the Sardana Follow-up Meeting - 2021/12/16

Held on Thursday 2021/12/16 at 14:00 CET

## Participants:
    - ALBA: Zibi
    - DESY: Teresa
    - MAXIV: Henrik
    - MBI: Daniel
    - SOLARIS: Michal

## Minutes

1. Presentation? Any proposal?
   - No proposals were received.
   - We shortly commented on the recent message from S2Innoviation 
     with a plenty of ideas for improvements - see [#1707](https://gitlab.com/sardana-org/sardana/-/issues/1707).
     Unfortunatelly Mateusz from S2Innovation had not arrived to the meeting so we could not talk directly to him.

2. Urgent user problems/issues - Round table

    - SOLARIS
        - No urgent issues
    - MAXIV
        - Experienced segfaults when defining elements due to [!1677](https://gitlab.com/sardana-org/sardana/-/merge_requests/1677).
          Their case was with the AlbaEM (electrometer) attributes which were read by the client and at the same time new elements were being
          defined in the Pool. During the definition of new elements, some attirbutes were being removed from the device class (due to the conflicting name)
          simultaneously to reading them, as a consequence it was crashing.
        - Has problems with `PrepareOne()` of slow detectors - see [#1717](https://gitlab.com/sardana-org/sardana/-/issues/1717).
          Their case is with a detector which needs to write a file to a NFS in the `PrepareOne()` and this sporadically takes too much time.
          Currently MeasurementGroup preparation is all executed in the Prepare command which has a limitation of 3 s by default - Tango CORBA timeout.
          There was an idea of making the preparation in the background job thread - similarly to the acquistiion. 
          Different phases of the measurement could be described by the new AcquisitionStatus - see [#1352](https://gitlab.com/sardana-org/sardana/-/issues/1352). The easiest implementation could be to map the AcquisitionStatus values to the Tango States e.g. Preparing, Starting, Acquiring and Reading to MOVING state.
    - DESY
        - No urgent issues
    - ALBA
        - Problems of strange crashes at BL13 (identified as a possible collission of `tango.AttributeProxy` destructor and the continuously failing stateless event subscription - see [pytango#302](https://gitlab.com/tango-controls/pytango/-/issues/302) seems to disappear after patching PyTango (3 weeks without problems). 
        - Issues with Ctrl+C at BL11 - to be verified
        - Issues with MacroServer environment permissions at IDLAB - to be verified
    - MBI
        - No major problems, just waiting for some issues to be solved:
            - Propagate keyboard interrupt exception [!767](https://gitlab.com/sardana-org/sardana/-/merge_requests/767) - see point 4 below
            - PseudoCounters based on another PseudoCounters fail to initialize [#823](https://gitlab.com/sardana-org/sardana/-/issues/823) - still on Zibi's TODO list
            - Reminded after the meeting: Duplicate alias on same tango hosts [#998](https://gitlab.com/sardana-org/sardana/-/issues/998)
                - it makes live really complicated in the lab with multiple Sardana setups which have similar hardware as we currently have 4 TangoDBs just to be able to use the same Motor and Counter names in all setups. 
        - How to properly store ROI information in NeXus files [#1711](https://gitlab.com/sardana-org/sardana/-/issues/1711) - see point 4 below
        - While discussing on "dial position vs. hardware position in Motor and Trigger/Gate controller API" reminded a feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344)
            - [ ] ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)

3. Review pending points from the previous meetings
    
    - From last meeting:
        - [ ] Zibi will set up meeting about Sardana configuration tool (discussion about solutions and text formats)
            - **no update**
        - GitHub -> GitLab migration
            - [x] pending to announce it
            - [x] renaming master is still todo
        - [x] Stored start-time should be when the scan starts, not when the macro starts (Henrik)
        - [x] Create issue to document problem with the Sequencer: launching ascan in ascan - `prepare is mandatory before starting acquisition` message
        - [ ] Create issue for a pseudomotors bug:
            - if you have pseudomotor with two physical motors on two different controllers and one is offline (i.e. no hardware
            communication) and you try to move the other one, it will hang 
            - reason is that before movement it is needed to read the position and as it has pseudomotor on top it hangs on 
            reading offline motor position
            - **no update**


    - From previous meetings:
        - [ ] Issue at MAX IV related to ADLINK controller from ALBA, related to the State. Seems this is only in use at some specific place at ALBA. 
            - Zibi will check with them.
                - [issue](https://github.com/ALBA-Synchrotron/sardana-adlink/issues/4) was created but people are out of office for now, more details may be needed
                - **Two pings were sent to ALBA colleagues and no reply. Remove it from the list of pending points.**
        - [x] Exotic problem with stateless subscriptions in PyTango that can cause issues in "fast dummy motor controller". Happens when Taurus tries to subscribe but fails. Can be caused by changing event config. No decision on how to proceed.
            - create issue in Sardana, in Pytango already created ([#302](https://gitlab.com/tango-controls/pytango/-/issues/302)) and discuss there
            - try to increase the priority in Pytango 
            - when fixed in Pytango no walkaround in Sardana needed
            - **we agreed to remove it from the list. When new PyTango/Tango releases are done we will close this issue so it disappears from Known Problems list.**
        - [ ] `self.<macro name>` exceptions are uninformative.
            - looked into and agreed but on all levels of traceback it is uninformative 
            - issue in Sardana and description of the traceback loops needed, then discussion about how it should work
            - **no update**
        - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
            - mentioned in round table
            - **no update. In this winter shutdown at ALBA we will try to reproduce it.**
        - [ ] Issue with Ctrl-C in spock in latest Sardana versions. The old behavior worked nicely with IPython, e.g. running macros in a loop, but due to how the signal is caught this no longer works.
            - Daniel's question 
            - there was an old PR doing this, we could be it now as well, but handling `Ctrl+C` is now more sophisticated
            - drawback: it will look ugly (`KeyboardInterrupt` exception in Spock)
            - **is already well advanced as a MR**
        - [ ] Problem with pseudocounters of pseudocounters. They work when first created, but not later (e.g. after restart). There is some problem with the order in which they get created.
            - Daniel's question, no update, issue created, probably a bug
            - **no udpate**
        - [ ] MacroServer startup hook (that would be called automatically after starting the server - an opposite to the at exit)
              - Zibi wanted to create a PR
              - **no update**
        - [ ] Users can break Sardana by messing with configuration in Tango DB:
     	    - status of the diagnostics script:
                - Sherlock presented - it will be added to the catalouge (but with a different name, now called `plugins`),
                - name should be generic
                - after discussion `sardana-extra` was selected
                - [x] Zibi will rename the project 
                - [ ] **Henrik will register sherlock in the extras catalogue**
        - [x] Hang scan due to the counter/timer controller timeout in StateOne() 
            - How to deal with exceptions in the controllers: Review the docs?
            - [ ] **make a MR with the proposal for updating docs: [#1693](https://gitlab.com/sardana-org/sardana/-/issues/1693)**
            - Henrik/MAXIV will look into adding functionality to the IcePap controller to show motor OFF state.
            - **no update**
        - [x] GUIs sardanaeditor and "showscan offline" will likely be removed from sardana as a result of low interest in the sardana user survey. 
            - [x] Zibi will make a PR.
            - **for the moment we agreed to remove only sardanaeditor. *showscan offline* was fixed.**
        
4. Overview of current developments / PR distribution

    - Multiplexor mode [#1678](https://gitlab.com/sardana-org/sardana/-/issues/1678)
        - Prior to the meeting, after discussions with Roberto, we agreed:
            - on passing dial position to the `SynchOne()` method but a backwards compatibility will be maintained.
            - Calculation of the DialPosition should be done only if necessary (controller with the capability to synchronized in the position domain)
            - Conversion factor will be excluded from the scope of this MR for the moment
        - During the meeting no comments against the above approach were raised.
        - We discussed on the reasons why Motor controller receives dial position and not the hardware position:
            - Daniel does not remember how exactly (dial vs. hardware) this is handed in the custom Motor controllers in Spec. He comments that there is a motor's configuration table in Spec where new motors are added to the system and the step_per_unit is defined. He comments is hard to get access such controllers code.
            - Teresa comments that they do not use sardana position conversion at DESY. They always delegate it to the underneath DS. She does not remember the discussions about this during the early times of Sardana.
    - Draft: Warn on conflict instead of remove and replace [!1677](https://gitlab.com/sardana-org/sardana/-/merge_requests/1677)
        - We agree to change state to FAULT when there is a conflict and set status to point the conflicting attribute names
        - Henrik is not sure if the change of ValueBuffer attribute type on the core level (str -> byte) was actually necessary.
            - [ ] Henrik will try the merge request without the byte change
    - Propagate keyboard interrupt exception [!767](https://gitlab.com/sardana-org/sardana/-/merge_requests/767)
        - [ ] Daniel will test it
        - Daniel asks if this would cover two special use cases:
            - load magic comment (lods ipy file)
            - one by one execution of macros in multi line cells (introduce with Ctrl+O - nice feature!) - we tested this one during the meeting and it does not work when aborting the first macro in the multine line cell
        - [ ] Zibi will try to solve it using the context field of the `FrameInfo` named tuple.
    - How to use NXinstrument properly [#1711](https://gitlab.com/sardana-org/sardana/-/issues/1711)
        - There are two decissions to be take, first:
            - Carlos proposal with 3 options to fix the inconsistency with NeXus (soft vs. hard links, dataset in instrument instead of measurement group)
                a) make it mandatory to provide the instrument info for all sardana elements (but then one needs to decide what to do for e.g. tango attributes recorded in the snapshots)
                b) create a dummy NXinstrument group (called e.g. "unknown") at the root of the instrument tree and just put there the datasets from sardana     elements not defining the instrument info
                c) store the datasets of elements not defining instrument info in the "measurement" group instead of linking to the instrument tree
            - Zibi propose to follow b) because:
                - b) will be easier to develop (all datasets are stored in the same way)
                - a) may be too strict (too complicated for someone starting with Sardana)
                - b) when browsing the NeXus file the "unknown" instrument group would pique user's curiosity and lead them to the instrumment configuration
            - Henrik comments that at MAXIV they use SciCat for the metadata storage
        - second:
            - When defining the pre scan snapshot we need a way to define where to store it in the NeXus path:
                - The exact path in the /instrument group could depend on:
                    1. If the pre scan snapshot attribute belongs to a Sardana element, it could reuse its instrument association
                    2. When adding an attribute to the pre scan snapshot one could optionally specify the NeXus Path that would eventually override the one from point 1.
        - We agreed to go for b) and implement the pre scan snapshot dataset location as for channels (store in instrument based on points 1. and 2.) and make soft links in the /measurement/pre_scan_snapshot group.
    - Simplify PseudoMotor configuration? [#1702](https://gitlab.com/sardana-org/sardana/-/issues/1702)
        - No comments agains removing these redundant properties
        - [ ] Henrik will work on a MR
    - Eliminate temporary motor groups [#740](https://gitlab.com/sardana-org/sardana/-/issues/740)
        - not discussed due to the lack of time

5. Sardana release 3.2 (Jul21 milestone)
    - Cent0S test done
    - Debian 10 - small issues to clarify; seems like no release blocking pending issues
    - Waiting for Win 10 and Debian 9
    - Probably will be release after holidays
    
6. AOB
    - Next Follow-up meeting will be held on the 27.01.2022, organized by DESY.
