# Agenda for the Sardana Follow-up Meeting - 2021/12/16

To be held on Thursday 2021/12/16 at 14:00

## Agenda

1. Presentation? Any proposal?

2. Urgent user problems/issues - Round table

    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - ...

3. Review pending points from the previous meetings
    
    - From last meeting:
        - [ ] Zibi will set up meeting about Sardana configuration tool (discussion about 
    solutions and text formats)
        - GitHub -> GitLab migration
            - [ ] pending to announce it
            - [ ] renaming master is still todo
        - [ ] Stored start-time should be when the scan starts, not when the macro starts (Henrik)
        - [ ] Create issue to document problem with the Sequencer: launching ascan in ascan - `prepare is mandatory before starting acquisition` message
        - [ ] Create issue for a pseudomotors bug:
            - if you have pseudomotor with two physical motors on two different controllers and one is offline (i.e. no hardware
            communication) and you try to move the other one, it will hang 
            - reason is that before movement it is needed to read the position and as it has pseudomotor on top it hangs on 
            reading offline motor position


    - From previous meetings:
        - [ ] Issue at MAX IV related to ADLINK controller from ALBA, related to the State. Seems this is only in use at some specific place at ALBA. 
            - Zibi will check with them.
                - [issue](https://github.com/ALBA-Synchrotron/sardana-adlink/issues/4) was created but people are out of office for now, more details may be needed
        - [ ] Exotic problem with stateless subscriptions in PyTango that can cause issues in "fast dummy motor controller". Happens when Taurus tries to subscribe but fails. Can be caused by changing event config. No decision on how to proceed.
            - create issue in Sardana, in Pytango already created ([#302](https://gitlab.com/tango-controls/pytango/-/issues/302)) and discuss there
            - try to increase the priority in Pytango 
            - when fixed in Pytango no walkaround in Sardana needed
        - [ ] `self.<macro name>` exceptions are uninformative.
            - looked into and agreed but on all levels of traceback it is uninformative 
            - issue in Sardana and description of the traceback loops needed, then discussion about how it should work
        - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
            - mentioned in round table
        - [ ] Issue with Ctrl-C in spock in latest Sardana versions. The old behavior worked nicely with IPython, e.g. running macros in a loop, but due to how the signal is caught this no longer works.
            - Daniel's question 
            - there was an old PR doing this, we could be it now as well, but handling `Ctrl+C` is now more sophisticated
            - drawback: it will look ugly (`KeyboardInterrupt` exception in Spock)
        - [ ] Problem with pseudocounters of pseudocounters. They work when first created, but not later (e.g. after restart). There is some problem with the order in which they get created.
            - Daniel's question, no update, issue created, probably a bug
        - [ ] MacroServer startup hook (that would be called automatically after starting the server - an opposite to the at exit)
              - Zibi wanted to create a PR
              - no update
        - [ ] Users can break Sardana by messing with configuration in Tango DB:
     	    - status of the diagnostics script:
                - Sherlock presented - it will be added to the catalouge (but with a different name, now called `plugins`),
                - name should be generic
                - after discussion `sardana-extra` was selected
                - Zibi will rename the project 
        - [x] Hang scan due to the counter/timer controller timeout in StateOne() 
            - How to deal with exceptions in the controllers: Review the docs?
            - Proposal for updating docs: [#1693](https://gitlab.com/sardana-org/sardana/-/issues/1693)
            - Henrik/MAXIV will look into adding functionality to the IcePap controller to show motor OFF state.
        - [ ] GUIs sardanaeditor and "showscan offline" will likely be removed from sardana as a result of low interest in the sardana user survey. 
            - Zibi will make a PR.
            - no update
        
4. Overview of current developments / PR distribution

    - Multiplexor mode [#1678](https://gitlab.com/sardana-org/sardana/-/issues/1678)
    - Draft: Warn on conflict instead of remove and replace [!1677](https://gitlab.com/sardana-org/sardana/-/merge_requests/1677)
    - Propagate keyboard interrupt exception [!767](https://gitlab.com/sardana-org/sardana/-/merge_requests/767)
    - How to use NXinstrument properly [#1711](https://gitlab.com/sardana-org/sardana/-/issues/1711)
    - Simplify PseudoMotor configuration? [#1702](https://gitlab.com/sardana-org/sardana/-/issues/1702)
    - Eliminate temporary motor groups [#740](https://gitlab.com/sardana-org/sardana/-/issues/1678)

5. Sardana release 3.2 (Jul21 milestone)
    
6. AOB
    - Next Follow-up meeting will be held on the XXX, organized by DESY.
