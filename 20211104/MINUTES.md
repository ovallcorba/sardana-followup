# Minutes from the Sardana Follow-up Meeting - 2021/11/04

To be held on Thursday 2021/11/04 at 14:00

Participants:
- MAXIV: Johan, Henrik
- ALBA: Zbignew, Marc, Saida
- DESY: Teresa
- SOLARIS: Michal, Tomasz

## Agenda

1. Presentation by Marc from ALBA of the Sardana integration with the Jupyter Lab
    - early state of the project: idea is to run macros in Jupiter
    - works in progress:
        - integrate live plots of the scans (right now only after scan is done we get a plot)
        - dropdown menus of i.e. available motors in scans - not to type everything to make 
        it easier for users
    - project available [here](https://gitlab.com/sardana-org/sardana-jupyter)
        - new ideas, help and participation is welcome
    - it introduces new architecture: we don't need to run Macroserver  as a Tango device
    (it is running inside a kernel)

2. Presentation by Tomek from SOLARIS of the Vue Spock

    - proof of concept at the moment, a lot of work to be done
    - idea is to easily embed into web apps
    - not published at the moment but we are willing to (possible licence limitations - to be checked)

3. Presentation by MAXIV of the `dsconfig` tool in the context of Sardana

    - idea is to create Sardana objects defined in the sheet (new objects ie. new motorisation)
    - features include: dumping Tangodb, applying new config from json, validation (shows diff)
    - available [here](https://gitlab.com/MaxIV/cfg-maxiv-sardanadsconfig)
    dsconfig lib [here](https://gitlab.com/MaxIV/lib-maxiv-dsconfig/-/tree/master/dsconfig) 
    - conclusion: Zibi will set up meeting about Sardana configuration tool (discussion about 
    solutions and text formats)

4. Urgent user problems/issues - Round table

    - SOLARIS
        - all beamlines migrated to Sardana3 - no more stability issues
        - monochromator - Insertion device issue [#1705](https://gitlab.com/sardana-org/sardana/-/issues/1705)
            - other institutes use IcePAP to move ID's motors
            - possible solutions:
                - adding retry mechanism
                - creating dedicated controller (replacing `TangoAttributeMotor` one)

    - MAXIV
        - problem with the Sequencer: launching ascan in ascan - `prepare is mandatory before starting acqisition` message
            - change in SEP18 - problem because the same Measurement Group for outer and inner scans is used and 
            `PrepareOne` method is called only once (not called back for outer one)
            - solution would be to use different Measurement Groups for those scans
            - issue is on the way to let Community know about the walkaround

    - DESY
        - no issues

    - ALBA
        - Measurement group configuration bug - when using disable/enable channel you can end up with error 
        - Scans hangs
            - running together two different Pools  - state attribute polling was the reason
            - silent crashes of the Macroserver
                - reason: subscribing to non existing attribute using Taurus in macros 
                - related to [#302](https://gitlab.com/tango-controls/pytango/-/issues/302) issue in Pytango
        - pseudomotors bug:
            - if you have pseudomotor with two physical motors on a two different controllers and one is offline (i.e. no hardware
            communication) and you try to move the other one, it will hang 
            - reason is that before movement it is needed to read the position and as it has pseudomotor on top it hangs on 
            reading offline motor position
        - issues for those cases are on the way    

5. Review pending points from the previous meetings
     - From last meeting:
        - [ ] Issue at MAX IV related to ADLINK controller from ALBA, related to the State. Seems this is only in use at some specific place at ALBA. 
            - Zibi will check with them.
                - [issue](https://github.com/ALBA-Synchrotron/sardana-adlink/issues/4) was created but people are out of office for now, more details may be needed
        - [ ] Exotic problem with stateless subscriptions in PyTango that can cause issues in "fast dummy motor controller". Happens when Taurus tries to subscribe but fails. Can be caused by changing event config. No decision on how to proceed.
            - create issue in Sardana, in Pytango already created ([#302](https://gitlab.com/tango-controls/pytango/-/issues/302)) and discuss there
            - try to increase the priority in Pytango 
            - when fixed in Pytango no walkaround in Sardana needed
        - [ ] `self.<macro name>` exceptions are uninformative.
            - looked into and agreed but on all levels of traceback it is uninformative 
            - issue in Sardana and description of the traceback loops needed, then discussion about how it should work
        - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
            - mentioned in round table
        - [ ] Issue with Ctrl-C in spock in latest Sardana versions. The old behavior worked nicely with IPython, e.g. running macros in a loop, but due to how the signal is caught this no longer works.
            - Daniel's question 
            - there was a old PR doing this, we could be it now as well, but handling `Ctrl+C` is now is more sophisticated
            - drawback: it will look ugly (`KeyboardInterrupt` exception in Spock)
        - [ ] Problem with pseudocounters of pseudocounters. They work when first created, but not later (e.g. after restart). There is some problem with the order in which they get created.
            - Daniel's question, no update, issue created, probably a bug
     - From previous meetings:
        - [ ] MacroServer startup hook (that would be called automatically after starting the server - an opposite to the atexit)
              - Zibi wanted to create a PR
              - no update
        - [ ] Users can break Sardana by messing with configuration in Tango DB:
     	    - status of the diagnostics script:
                - Sherlock presented - it will be added to the catalouge (but with a different name, now called `plugins`),
                - name should be generic
                - after discussion `sardana-extra` was selected
                - Zibi will rename the project 
        - [x] Hang scan due to the counter/timer controller timeout in StateOne() 
            - How to deal with exceptions in the controllers: Review the docs?
            - Proposal for updating docs: [#1693](https://gitlab.com/sardana-org/sardana/-/issues/1693)
            - Henrik/MAXIV will look into adding functionality to the IcePap controller to show motor OFF state.
        - [ ] GUIs sardanaeditor and "showscan offline" will likely be removed from sardana as a result of low interest in the sardana user survey. 
            - Zibi will make a PR.
            - no update
        
6. Overview of current developments / PR distribution
    - Multiplexor mode [#1678](https://gitlab.com/sardana-org/sardana/-/issues/1678)
        - in progress, if you have any preferences please comment in the issue
    
7. Migration to gitlab and rename master branch to main/stable.
    - From the last meeting
        - Mainly Carlos and Benjamin will perform the migration 
            - Benjamin migrated CI part
        - Carlos will make announcement, leaving at least two weeks for users to respond
        - Bunch of other things to also migrate
        - Documentation build is currently broken anyway
    - migration done, all repos are in Gitlab
    - pending to announce it
    - renaming master is still todo

8. Sardana release 3.2 (Jul21 milestone)
    - From the last meeting
        - Need to wait for Taurus to make a release ~2 weeks.
        - Solaris and DESY are supposed to handle this release
        - The bug squashing party merged an issue that also requires some discussion
        - [ ] Stored start-time should be when the scan starts, not when the macro starts (Henrik)
            - no update
        - Various things left open from the bug squashing party for the future.
    - still not done, waiting for Taurus 5
    
9. AOB
    - no issues
    
Next Follow-up meeting will be held on the 16th of December at 14:00, organized by ALBA.
