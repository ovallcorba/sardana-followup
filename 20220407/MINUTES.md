# Minutes for the Sardana Follow-up Meeting - 2022/04/07

Held on Thursday 2022/04/07 at 14:00

Present:
- Zibi (ALBA)
- Michal (SOLARIS)
- Teresa (DESY)
- Daniel (MBI)

## Agenda

1. Discussion on limit switches - see [#1735](https://gitlab.com/sardana-org/sardana/-/issues/1735)
   - informing user what will happen after execution is crutial
   - interactive macro asking to change limits while setting position is a good idea, its default should be to `yes`
   - changing `sign` - should be implemented the same way? also a macro? discussion needed, in [#159](https://gitlab.com/sardana-org/sardana/-/issues/159)
   - discussion also needed in terms of `step_per_unit`, there are also a few cases (microstepping or actual unit change), in [#159](https://gitlab.com/sardana-org/sardana/-/issues/159)
   - MBI and SOLARIS found separate limits (`dial`, `user`) good and take advantage from that 
   - should setting `dial` limits also set it in the hardware, issue [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344)
       - generally yes
       - not so trivial to implement
   - all issues/ideas should be documented in one place, Zibi will document it in [#159](https://gitlab.com/sardana-org/sardana/-/issues/159) and this will be the main place for discussion on this matter
   - issue raised about macro names for setting limits unification [#1641](https://gitlab.com/sardana-org/sardana/-/issues/1641)
     - agreed on that
     - Daniel will propose a MR for the naming
   - however, all these tasks are not of top priority

2. Sardana Configuration Tool - see [!1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
   - at the moment, institutes have some tools for at least partial configuration or environment handling tools/macros
   - users should be able to use it comfortably
   - dedicated Sardana Configuration Tool brainstorm meeting will be held on 12.05.2022 at 14:00
     - first we need to set requirements, analyze configuration points (i.e environment - Zibi will prepare it) and set priorities
     - seeing what is possible with different tools (from different institutes, SPECS...) could be beneficial before/during the meeting

3. Urgent user problems/issues - Round table

    - SOLARIS
      - no issues
    - DESY
      - no issues
    - ALBA
      - no urgent issues
      - upgrading Sardana instances to 3.2.1
      - Elmo motor question still pending, no answer from MAXIV
      - configuration issue about removing measurement group global timer/monitor [#1736](https://gitlab.com/sardana-org/sardana/-/issues/1736)
    - MBI
      - Duplicate alias on same tango host - [#998](https://gitlab.com/sardana-org/sardana/-/issues/998)
        - using prefixes may be an option
        - DESY uses different DBs
      - no additional issues

4. Review pending points from the previous meetings

   - From last meeting:
     - MAXIV
       - [ ] Issue with extra axis attributes on pseudo motor not causing change events on position
         https://github.com/sardana-org/sardana/issues/1693 <- related
         - no update
       - [X] Sardana 3.2 is not compatible with python 3.10 
       conda installation broken unless older python version specified.
       This is fixed in develop, due to https://gitlab.com/sardana-org/sardana/-/merge_requests/1722
       Propose to release a "hotfix" version 3.2.1 https://gitlab.com/sardana-org/sardana/-/wikis/How-to-Hotfix-Sardana 
       with this backported.
       Zibi will test a bit to see that there are no obvious issues with 3.10. Then we schedule creating the hotfix release.
         - latest hotfix (3.2.1) solves it 
     - ALBA
       - [X] Other 3.2 issue, Tango attribute controller has a very long timeout (can be 100000 s) for attributes that don't implement 
         the "MOVING" quality, that can can prevent aborting macros. It can be configured with a speed attribute but may need 
         tuning per attribute. Proposed solution is to allow aborting.
         - no update, Zibi will create an issue 
        
   - From previous meetings:

       - [ ] Issue with configurationn of dynamic attributes, mostly on the tango layer. Henry is working on that: sending
      a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
      Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
      example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.
         - no update 

       - [X] Zibi will set up meeting about Sardana configuration tool (discussion about solutions and text formats)
         - done, as stated above, dedicated Sardana Configuration Tool brainstorm meeting will be held on 12.05.2022 at 14:00
            
       - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
           - mentioned in round table
           - no update, need to reproduce 
            
       - [ ] Users can break Sardana by messing with configuration in Tango DB:
            - status of the diagnostics script:
               - [ ] Henrik wanted to register sherlock in the extras catalogue
               - no update 

       - [ ] For feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344):
           - ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)
           - no update

5. Overview of current developments / MR distribution
   - `reconfig` macro, in order to work with single Sardana server, requires [cppTango!907](https://gitlab.com/tango-controls/cppTango/-/merge_requests/907)
     - do we need it for cppTango 9.3.x or 9.4.0 (end of 2022) would be enough? 
     - no urgency of [!873](https://gitlab.com/sardana-org/sardana/-/merge_requests/873) so in 9.4.0 it would be enough
   - Load sardanacustomsettings from config files - [!1733](https://gitlab.com/sardana-org/sardana/-/merge_requests/1733)
     - Should we follow XDG or not?
     - no strong preference/experience but following Taurus would be a good idea
   - Fix order of restoring memorized attribute values [!1744](https://gitlab.com/sardana-org/sardana/-/merge_requests/1744)
     - ready for review
     - order documented here: [#1732](https://gitlab.com/sardana-org/sardana/-/issues/1732)
   - Introduced two modes: edit and visualization in ExperimentConfiguration UI - [!1739](https://gitlab.com/sardana-org/sardana/-/merge_requests/1739)
     - problem with scrolling in view mode
     - make it browsable but not editable (QT issue)
   - Fix DiscretePseudoMotor when its position is out of calibration range [!1740](https://gitlab.com/sardana-org/sardana/-/merge_requests/1740)
   - 1D channels in SPEC header (storage files) [!1746](https://gitlab.com/sardana-org/sardana/-/merge_requests/1746)

6. MR integration and approval rules
   - Ping integrators when there is a new MR for review. Let's see if it works.
     - pinging works very well
   - Discuss further the usage of a "kanban" or similar for tracking development status.
     - postpone it for now 

7. Sardana release
   - Next version probably 3.3, released summer 2022
     - Milestones changed accordingly
     - next is Jun22
    
8. AOB
   - Meeting Platform
     - Last time we had problems with Jitsi, if today it still cause issues,
       maybe we should start thinking about an alternative?
       - this time it worked well, last time it was probably an internet connection issue
   - how we respond to Taurus?
     - we try to help with Taurus issues as a Sardana community until a new person responsible from ALBA is selected 
   - Next meeting?
     - Next meeting 02.06.2022, organized by ALBA 
