# Agenda for the Sardana Follow-up Meeting - 2022/04/07

To be held on Thursday 2022/04/07 at 14:00

## Agenda

1. Discussion on limit switches - see [#1735](https://gitlab.com/sardana-org/sardana/-/issues/1735)
2. Sardana Configuration Tool - see [!1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
3. Urgent user problems/issues - Round table

    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - MBI
      - Duplicate alias on same tango host - [#998](https://gitlab.com/sardana-org/sardana/-/issues/998)
    - ...

4. Review pending points from the previous meetings

   - From last meeting:
     - MAXIV
       - [ ] Issue with extra axis attributes on pseudo motor not causing change events on position
         https://github.com/sardana-org/sardana/issues/1693 <- related
       - [ ] Sardana 3.2 is not compatible with python 3.10 
       conda installation broken unless older python version specified.
       This is fixed in develop, due to https://gitlab.com/sardana-org/sardana/-/merge_requests/1722
       Propose to release a "hotfix" version 3.2.1 https://gitlab.com/sardana-org/sardana/-/wikis/How-to-Hotfix-Sardana 
       with this backported.
       Zibi will test a bit to see that there are no obvious issues with 3.10. Then we schedule creating the hotfix release.
     - ALBA
       - [ ] Other 3.2 issue, Tango attribute controller has a very long timeout (can be 100000 s) for attributes that don't implement 
         the "MOVING" quality, that can can prevent aborting macros. It can be configured with a speed attribute but may need 
         tuning per attribute. Proposed solution is to allow aborting.
        
   - From previous meetings:

       - [ ] Issue with configurationn of dynamic attributes, mostly on the tango layer. Henry is working on that: sending
      a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
      Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
      example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.

       - [ ] Zibi will set up meeting about Sardana configuration tool (discussion about solutions and text formats)
            
       - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
           - mentioned in round table
            
       - [ ] Users can break Sardana by messing with configuration in Tango DB:
            - status of the diagnostics script:
               - [ ] Henrik wanted to register sherlock in the extras catalogue

       - [ ] For feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344):
           - ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)
          

5. Overview of current developments / MR distribution
   - `reconfig` macro, in order to work with single Sardana server, requires [cppTango!907](https://gitlab.com/tango-controls/cppTango/-/merge_requests/907)
     - do we need it for cppTango 9.3.x or 9.4.0 (end of 2022) would be enough?
   - Load sardanacustomsettings from config files - [!733](https://gitlab.com/sardana-org/sardana/-/merge_requests/1733)
     - Should we follow XDG or not?
   - Fix order of restoring memorized attribute values [!1744](https://gitlab.com/sardana-org/sardana/-/merge_requests/1744)
   - Introduced two modes: edit and visualization in ExperimentConfiguration UI - [!1739](https://gitlab.com/sardana-org/sardana/-/merge_requests/1739)
   - Fix DiscretePseudoMotor when its position is out of calibration range [!1740](https://gitlab.com/sardana-org/sardana/-/merge_requests/1740)
   - 1D channels in SPEC header (storage files) [!1746](https://gitlab.com/sardana-org/sardana/-/merge_requests/1746)

6. MR integration and approval rules
   - Ping integrators when there is a new MR for review. Let's see if it works.
   - Discuss further the usage of a "kanban" or similar for tracking development status.

7. Sardana release
   - Next version probably 3.3, released summer 2022
    
8. AOB
   - Meeting Platform
     - Last time we had problems with Jitsi, if today it still cause issues,
       maybe we should start thinking about an alternative?
   - Next meeting?
