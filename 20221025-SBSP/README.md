---
title: Sardana Bug Squashing Party (SBSP) 2022
tags: Talk
description: View the slide with "Slide Mode".
---

# Sardana Bug Squashing Party (SBSP) 2022

<!-- Put the link to this slide here so people can follow -->
slide: https://hackmd.io/@reszelaz/ry4ZmoSXj

---

## Objectives

- Meet again or get to know new faces, this time also in person!
- Share knowledge by participating in pair-programming sessions, peer reviews, etc. 
- Improve the Sardana project
- Reduce very long backlog of issues: ~300

---

## [Agenda](https://gitlab.com/sardana-org/sardana-followup/-/blob/main/20221025-SBSP/AGENDA.md)

---

## Presentation of participants

---

## SBSP 2022 pre-work

- Classification of issues using labels e.g. storage, scan, motion, daq, etc.
- Estimation of issues: size (S, M, L, XL) and impact (low, medium, high)
- Pre-selection of issues:
    - size:S & impact:high
    - size:S & impact:medium
    - good first issue
- Some extras...

---

## SBSP 2022 organization

----

### [Zoom meeting](https://rediris.zoom.us/j/89498033909?pwd=d3ZhWmFJWTJZZzBoeFl3LzJuRGwyQT09)

- Meeting ID: 894 9803 3909 & Passcode: 054154 
- Breakout rooms (for pair-programming & coffee-breaks)
- You all are co-host (in order to have a pre-view of breakout rooms)
- Chat for: asking for review, calling for help, etc.

----

### [GitLab board](https://gitlab.com/sardana-org/sardana/-/boards/3529956)

- Already used during the previous SBSP2021
- Board only for issues, not possible to add MRs
- You will need to be sardana-org/sardana project member to really use it

----

### [GitLab board](https://gitlab.com/sardana-org/sardana/-/boards/3529956)

- Kanban board with the following phases:
    - *In design* -> *In design review* -> *In dev* -> *In review*
- ... and the following buffer columns
    - *To desing*, *To review design*, *To develop* and *To review develop*
- Let's use pull model:
    - take the first free card starting from the right-most column

----

### Development environment

1. [Use conda environment](https://sardana-controls.org/users/getting_started/installing.html#working-from-git-source-directly-in-develop-mode) and Tando DB run on your host 
2. Use Sardana `.devcontainer` with [Visual Studio Code](https://code.visualstudio.com/) IDE
3. Use [sardana-docker](https://gitlab.com/sardana-org/sardana-docker) *all-in-one* Docker container (still based on Debian Stretch)

----

### Pair programming

[Some options](https://gitlab.com/sardana-org/sardana-followup/-/issues/26), e.g:
1. PyCharm's Code With Me extension
2. VSC LiveShare extension
3. [tmate](https://) - instant terminal sharing

---

## [Development guidelines](https://gitlab.com/sardana-org/sardana/-/jobs/artifacts/develop/file/public/index.html?job=build-doc)

Documentation -> Developer's Guide -> Development guideline

---

## [Pool core test fixtures](https://gitlab.com/sardana-org/sardana/-/jobs/artifacts/develop/file/public/index.html?job=build-doc)

Documentation -> Developer's Guide -> API -> Library -> sardana -> pool -> test

---

## Summary

- Real objective: learn from each other & have fun!
- Let's keep the project board updated to facilitate collaboration
- It would we nice if after this event you could feel confident and familiar with Sardana to report issues or even provide MR :)

---

## Thanks!
