# Sardana Bug Squashing Party (SBSP) 2022

## Where?

Zoom Meeting URL: https://rediris.zoom.us/j/89498033909?pwd=d3ZhWmFJWTJZZzBoeFl3LzJuRGwyQT09

(Meeting ID: 894 9803 3909 Passcode: 054154)

## Agenda

### Tuesday 25.10.2022 (afternoon only)
```
14:00 - 14:15 - SBSP 2022 format presentation
14:15 - 14:30 - Sardana architecture reminder (from follow-up meeting 20221013)
14:30 - 16:00 - Squashing Bugs
16:00 - 16:30 - Coffee Break :) 
16:30 - 18:00 - Squashing Bugs
```
### Wednesday 26.06.2021
```
 9:00 -  9:15 - Update meeting
 9:15 - 11:00 - Squashing Bugs
11:00 - 11:30 - Coffee Break :)
11:15 - 13:00 - Squashing Bugs

14:00 - 14:15 - Update meeting
14:15 - 16:00 - Squashing Bugs
16:00 - 16:30 - Coffee Break :) 
16:30 - 18:00 - Squashing Bugs
```
### Wednesday 27.06.2021
```
 9:00 -  9:15 - Update meeting
 9:15 - 11:00 - Squashing Bugs
11:00 - 11:30 - Coffee Break :)
11:15 - 13:00 - Squashing Bugs

14:00 - 14:15 - Update meeting
14:15 - 16:00 - Squashing Bugs
16:00 - 16:30 - Coffee Break :) 
16:30 - 18:00 - Squashing Bugs
```
### Friday 28.06.2021 (morning only)
```
 9:00 -  9:15 - Update meeting
 9:15 - 11:00 - Squashing Bugs
11:00 - 11:30 - Coffee Break :)
11:15 - 12:45 - Squashing Bugs
12:45 - 13:00 - Summary and closing
```
