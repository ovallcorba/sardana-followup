# Minutes from the Sardana Follow-up Meeting - 2023/09/28

Held on Thursday 2023/09/28 at 14:00

Organizer: Michal @ SOLARIS

Participants: Vanessa, Zibi, Michal, Teresa, Oriol, Roberto, Mirjam, Vincent

## Agenda

0. Sardana Roadmap - extra discussion
   - Zibi introduced the topic, all points gathered [here](https://docs.google.com/document/d/1Oc27R5pHt4b9J5o5Feh3ea8VC25xKVK3IA5WvS9hYJM/edit?usp=sharing)
   - discussion over all points, some new added, order evaluated
     - MAX IV voted for different synchronization description but without producing data (sample environment, laser, etc.)
     - recovery: repeating point discussion
       - general condition for step scans already there, [#1481](https://gitlab.com/sardana-org/sardana/-/merge_requests/1481)
         - how to pause macro [#1784](https://gitlab.com/sardana-org/sardana/-/issues/1784)
       - for now this solution can be tested for step scans
       - eventually: user-friendly configuration of recovery behaviour would be an expected feature
   - suggestion: set milestone for the next release and assign short terms issues to it + add assignees

1. Sardana Continuous Scans Workshop @ SOLARIS
    - summary and conclusions
    - blissdata 1.0 demo poll
      - point 0 as a summary

2. ICALEPCS2023 contribution
    - status update
    - help needed?
      - almost ready, roadmap to be added + some polishing

3. Jul23 release status
   - experiment status widget already in develop
   - there will be a release once the configuration tasks are completed
   - probably soon after ICALEPCS23

4. SEP20 status
   - skipped

5. Operation highlights, user's feedback, issues, etc. - Round table
   
    - ALBA
      - configuration dump with discrete pseudomotors produces validation errors on loading (type errors) - need to be tested more, if confirmed, issue will be created
      - timescanct idx issue - spotted on one beamline, further tests needed
    - DESY
      - Teresa had to left ealier
    - MAXIV
      - upgraded older Sardana instances to 3.4
      - some user used motorgroups in macros, after upgrade macro crashed - users should not use such elements directly. For now MAX IV will instruct users not to use them
    - MBI Berlin
      - not present
    - SOLARIS
      - missing aliases of elements (Pool, MacroServer, Door...) crashes configuration dump - Michal will create an issue
       
6. Review pending points from the previous meetings

  - From last meeting
  - From previous meetings:
    - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab
      - MAX IV has to publish some repos (including Scan GUI) - will do this soon
    - [Experiment status widget](https://gitlab.com/sardana-org/sardana/-/issues/1489):
      - [x] Michal will make a MR for the `wa` macro with regard to reserved elements
        - Reproduced the issue, a MR is upcoming.
          - MR accepted and merged
     
7. Overview of current developments / MR distribution
   - Review project board
      - skipped

8. Review of the oldest 15 issues in the backlog
   - [#68](https://gitlab.com/sardana-org/sardana/-/issues/68) - Michal will try to reproduce, keeping
   - [#81](https://gitlab.com/sardana-org/sardana/-/issues/81) - labelled, we need to increase the "Ready to accept requests" or similar message level to info, keeping
   - [#86](https://gitlab.com/sardana-org/sardana/-/issues/86) - there is need for it, we keep it open (MBI contribution?)
   - [#93](https://gitlab.com/sardana-org/sardana/-/issues/93) - keeping it as for now walkaround is hacky, labelled
   - [#97](https://gitlab.com/sardana-org/sardana/-/issues/97) - marked as stale and closed
   - [#107](https://gitlab.com/sardana-org/sardana/-/issues/107) - marked as stale and closed

9. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.
     - Needs discussion of pros and cons.
       - skipped
   - open source licence for Sardana project on Gitlab expired
     - after contact with Gitlab they renew it for the next year
   - Next meeting?
     - 9.11.2023 at 14:00, organized by ALBA
