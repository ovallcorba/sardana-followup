# Agenda for the Sardana Follow-up Meeting - 2022/01/27

To be held on Thursday 2022/01/27 at 14:00

## Agenda

1. Urgent user problems/issues - Round table

    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - ...

2. Review pending points from the previous meetings

    - From last meeting:
        - [ ] For feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344):
            - ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)
	    
        - [ ] Simplify PseudoMotor configuration? [#1702](https://gitlab.com/sardana-org/sardana/-/issues/1702)
            - Henrik will work on a MR
    
    - From previous meetings:
        - [ ] Zibi will set up meeting about Sardana configuration tool (discussion about solutions and text formats)
        - [ ] Create issue for a pseudomotors bug:
            - if you have pseudomotor with two physical motors on two different controllers and one is offline (i.e. no hardware
            communication) and you try to move the other one, it will hang 
            - reason is that before movement it is needed to read the position and as it has pseudomotor on top it hangs on 
              reading offline motor position
        - [ ] `self.<macro name>` exceptions are uninformative.
            - looked into and agreed but on all levels of traceback it is uninformative 
            - issue in Sardana and description of the traceback loops needed, then discussion about how it should work
        - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
            - mentioned in round table
        - [ ] Issue with Ctrl-C in spock in latest Sardana versions. The old behavior worked nicely with IPython, e.g. running macros in a loop, but due to how
	      the signal is caught this no longer works.
	    - advanced MR about it
        - [ ] Problem with pseudocounters of pseudocounters. They work when first created, but not later (e.g. after restart).
	      There is some problem with the order in which they get created.
            - Daniel's question,  issue created, probably a bug
        - [ ] MacroServer startup hook (that would be called automatically after starting the server - an opposite to the at exit)
              - Zibi wanted to create a MR
        - [ ] Users can break Sardana by messing with configuration in Tango DB:
     	    - status of the diagnostics script:
                - [ ] Henrik wanted to register sherlock in the extras catalogue
        - [ ] Hang scan due to the counter/timer controller timeout in StateOne() 
            - How to deal with exceptions in the controllers: Review the docs?
            - [ ] make a MR with the proposal for updating docs: [#1693](https://gitlab.com/sardana-org/sardana/-/issues/1693)
            - Henrik/MAXIV will look into adding functionality to the IcePap controller to show motor OFF state.
	    
3. Overview of current developments / MR distribution

    - Multiplexor mode [#1678](https://gitlab.com/sardana-org/sardana/-/issues/1678)
    - Draft: Warn on conflict instead of remove and replace [!1677](https://gitlab.com/sardana-org/sardana/-/merge_requests/1677)

4. MR integration and approval rules

5. Sardana release 3.2 (Jul21 milestone)
    
6. AOB
