# Minutes for the Sardana Follow-up Meeting - 2022/01/27

Held on Thursday 2022/01/27 at 14:00

## Participants:
    - ALBA: Zibi
    - DESY: Teresa
    - MAXIV: Henrik, Johan, Vincent
    - SOLARIS: Michal

## Minutes

1. Urgent user problems/issues - Round table

    - SOLARIS:
       No issues,
    - MAXIV:
       Issue with configurationn of dynamic attributes, mostly on the tango layer. Henry is working on that: sending
       a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
       Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
       example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.
    - DESY:
       No issues.
    - ALBA:
       New requirement for continuous scans: configure the timeout for acquisition, now value fixed to 15 sec (after the movement, for
       avoiding scans hanging for ever). Roberto will create an issue and MR for that.
    - ...

2. Review pending points from the previous meetings

    - From last meeting:
        - [ ] For feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344):
            - ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)
	    It is about how to pass the limits to the hardware, one idea is to use the tango interface to set limits, and we wonder
	    if there is a way on Tango in the server side to tell the client that there was a change in the range.
	    No update on this.
	    
        - [ ] Simplify PseudoMotor configuration? [#1702](https://gitlab.com/sardana-org/sardana/-/issues/1702)
            - Henrik will work on a MR
	    Henrik will not work on that, since he will leave MAXIV at the end of the month.
            No update on this.
	    
    - From previous meetings:
        - [ ] Zibi will set up meeting about Sardana configuration tool (discussion about solutions and text formats)
	    The idea is create a common tool evaluating the existing ones.
	    No update on this.
	    
        - [x] Create issue for a pseudomotors bug:
            - if you have pseudomotor with two physical motors on two different controllers and one is offline (i.e. no hardware
            communication) and you try to move the other one, it will hang 
            - reason is that before movement it is needed to read the position and as it has pseudomotor on top it hangs on 
              reading offline motor position
              An issue is created [#1720](https://gitlab.com/sardana-org/sardana/-/issues/1720) but the fix is still not proposed.
	      It should not be difficult to implement, but it should be tested.
	      
        - [ ] `self.<macro name>` exceptions are uninformative.
            - looked into and agreed but on all levels of traceback it is uninformative
            - issue in Sardana and description of the traceback loops needed, then discussion about how it should work
	      Not updates on that.
	      
        - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
              It was observed in Alba and they will try to reproduce it, if it is related to polling it will be reported to Tango.
	      They use a workaround disabling the polling, and like this it works.
	      Not updates on that
	      
        - [x] Issue with Ctrl-C in spock in latest Sardana versions. The old behavior worked nicely with IPython, e.g. running macros
	      in a loop, but due to how the signal is caught this no longer works.
	      MR is merged and documented in the spock documentation (chapter "Executing macro sequences"), the sequences can now
	      be stoped, not only the running macro but also all the subsequent ones.
	      
        - [ ] Problem with pseudocounters of pseudocounters. They work when first created, but not later (e.g. after restart).
	      There is some problem with the order in which they get created.
            - Daniel's question,  issue created, probably a bug
	      No updates on that.
	      
        - [ ] MacroServer startup hook (that would be called automatically after starting the server - an opposite to the at exit)
              - Zibi wanted to create a MR
	      Zibi created an issue [#1721](https://gitlab.com/sardana-org/sardana/-/issues/1721).
	      Exampls of  use could be the nexus configuration or  the preparation of the measurement
	      groups if it takes more that 3 sec. For this last point there is a macro in Alba, setting up a dedicated timeout
	      (using the Taurus device to set an extended timeout to the underlying tango server), but calling the macro could be forgotten,
	      an macroserver hook will be better. At DESY a python script is called by the DESY configuration tool after the
	      MacroServer is started, the users can define there what they need.
	      
        - [ ] Users can break Sardana by messing with configuration in Tango DB:
     	    - status of the diagnostics script:
                - Johan will register sherlock in the extras catalogue
	      
        - [x] Hang scan due to the counter/timer controller timeout in StateOne() 
            - How to deal with exceptions in the controllers: Review the docs?
            - [x] make a MR with the proposal for updating docs: [#1693](https://gitlab.com/sardana-org/sardana/-/issues/1693)
            - Henrik/MAXIV will look into adding functionality to the IcePap controller to show motor OFF state.

 	    Henrik worked on that, he will add a link from the documentation to the IcePap controller.
	    The MR about the documentation is done.
	    Guiffre made some comments about the states.
	    
3. Overview of current developments / MR distribution

    - Multiplexor mode [#1678](https://gitlab.com/sardana-org/sardana/-/issues/1678)
      The MR was approved and it could be merged.

    - Issue [#1724](https://gitlab.com/sardana-org/sardana/-/issues/1724) was explained by Zibi, it introduces
      controller API version specification and validation. It could be complicated for the developer, one needs
      to specify a version of sardana in the controller.

    - Draft: Warn on conflict instead of remove and replace [!1677](https://gitlab.com/sardana-org/sardana/-/merge_requests/1677)
      Henrik explained the idea and the limitations. The discussion is in the MR. It has still to be worked on that.
      They will try to reproduce the errors outside of the beamline.

    - Memorized attributes. There is a problem with the default values, they are written as memorized attributes, if it is changed
      in the code it will not be taken. It was agree to decouple those things and do not rely on tango for storing the values.
      A MR was prepared for that [!1730](https://gitlab.com/sardana-org/sardana/-/merge_requests/1730).
      There a feature request related to this, the reconfig. There is a MR for the reconfig macro
      [!873](https://gitlab.com/sardana-org/sardana/-/merge_requests/873). The problem is that the configuration is not refresh
      in the init_device, but in the start of the server. reconfig should also apply to the memorized values.
      Zibi plans working on the memorized after the reconfig.

    - Sardana Jupiter project is done and could be tested.

4. MR integration and approval rules.
     Michal and Zibi found a problem with gitlab approving the MR. It would be nice to define approval rules and
     with that change the gitlab settings accordingly. There are now one or two integrators per institute. The MR
     stay quite some time without review, in most of the cases nobody is mentioned for reviewing it. It could be discussed
     before the MR, and like this the review would be easier. An idea is to create different phases of developing, and pulling
     from one state to the other when there is actually people to work on that. Zibi has a first design of that, with
     already some stuff.
     There is a also a large list of merge requests that should be distributed.
     Some institutes do not have time for reviewing. It could be put a limit on time, and if nobody comments before the
     MR could be merged. Some MR affect some other work, and it would be nice to review them as soon as possible.
     One somebody takes a MR should assigned it to himself as a reviewer.

5. Sardana release 3.2 (Jul21 milestone)
     All the tests are done, so the release process could be continued.
    
6. AOB
    - Carlos Pascual asks for review and acceptance of TEP20.
    - Next Follow-up meeting will be held on the 03.03.2022, organized by MAXIV.


