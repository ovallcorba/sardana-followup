# Agenda for the Sardana Follow-up Meeting - 2022/11/17

To be held on Thursday 2022/11/17 at 14:00

## Agenda

1. Feedback on Sardana Bug Squashing Party 2022
  
2. Operation highlights, user's feedback, issues, etc. - Round table
   
    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - MBI
    - ...

3. Review pending points from the previous meetings

   - From last meeting:
     - [ ] spock json encoding warnings after running `expconf` - more investigation needed, no issue created yet

   - From previous meetings:
       - [ ] Hangs on Pool shutdown if there are event subscriptions present
         - no update x1, no issue yet
          
4. Overview of current developments / MR distribution
   - SEP20 works [#1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749), [#1802](https://gitlab.com/sardana-org/sardana/-/merge_requests/1802), ...
   - Protect against exceptions in hardware acquisition and sychronization start_action() [!1838](https://gitlab.com/sardana-org/sardana/-/merge_requests/1838)
   - Review project board

5. Review of the oldest 15 issues in the backlog

6. AOB
   - Next meeting?
