# Minutes from the Sardana Follow-up Meeting - 2022/11/17

Held on Thursday 2022/11/17 at 14:00

## Participants:
    Johan, Michal, Teresa, Zibi
## Minutes

1. Feedback on Sardana Bug Squashing Party 2022
   - Zibi's feedback: event hybrid format is a challenge for the organizer
     it was difficult to pay attention to issues raised by local and remote
     participants.
   - Positive feedback from Solaris.
   - Teresa due to technical problems could not attend Zoom, but was developing
     on her own.
  
2. Operation highlights, user's feedback, issues, etc. - Round table
   
    - SOLARIS
      - shutdown, extending the building
      - plan to update to the latest sardana, but Taurus 5 may be blocking
        mainly due to Qt5
    - MAXIV
      - no update, Johan involved in archiving
    - DESY
      - no issues with Sardana
    - ALBA
      - All beamlines but one running 3.3.x
      - Local changes at one beamline:
        - protecting against NaN in scan files
        - protecting against exceptions in `start_action()`
      - No possibility to execute AbortMacro from macroexecutor/ sequencer
      - Hangs on Pool shutdown due to Taurus unsubscribing from events in atexit hook? Need to kill twice in astor.
        - Similar things happening at MaxIV, but not sure if the root cause is the same
      - Slow data extraction/transfer/storage causing dead time in very fast continuous scans
    - MBI
      - looking forward to test "duplicate alias on same tango host" and "general condition" features

3. Review pending points from the previous meetings

   - From last meeting:
     - [x] spock json encoding warnings after running `expconf` - more investigation needed, no issue created yet
       - issue created and closed cause was due to a network infrastructure problem at ALBA [#1797](https://gitlab.com/sardana-org/sardana/-/issues/1797)

   - From previous meetings:
       - [ ] Hangs on Pool shutdown if there are event subscriptions present
         - no update x2, no issue yet
         - happens at more beamlines at ALBA, we try to patch it locally, but we should probably report to Tango
          
4. Overview of current developments / MR distribution
   - SEP20 works
     - [!1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749),
     - [!1802](https://gitlab.com/sardana-org/sardana/-/merge_requests/1802)
       - Zibi has an idea on how to implement it - reuse Event-Subscriber - dependent elements are connected
         and change the property in the Tango class listener
     - [#1775](https://gitlab.com/sardana-org/sardana/-/issues/1775) - to be postponed after Jan23
   - Protect against exceptions in hardware acquisition and sychronization start_action() [!1838](https://gitlab.com/sardana-org/sardana/-/merge_requests/1838)
     - [ ] Meet Teresa and Zibi tomorrow at 10
   - Review project board
     - Variable number of scan points depending on user defined condition [#201](https://gitlab.com/sardana-org/sardana/-/issues/201)
        - `GeneralCondition` we go for env variable, we will be always on time to add a hook place and deprecate the env variable.
        - if the general condition macro rerutn bool (repeat or not) then we we will always store the data, returning a dict will enable a possibility to not store
        - we will add it to all scans - `fscan` and `mesh` - should be copy and paste
        - Documentation:
          - in scans
          - in environment variable catalog
        - [ ] Teresa will implement the missing points
     - Pool should not log an ERROR if hkl is not installed - #394
       - [ ] Michal will ping Wojciech       
     - scanstats for 2d/3d scans
       - [ ] Teresa will make the final review
     - spock takes too much time to write data 
       - [ ] we will try to reproduce it at ALBA
     - Change the default behaviour of expconf view to the simple one
       - [ ] Zibi will ping Jordi
     - call newfile macro in expconf storage dialog
       - We ping S2Innovation
     - PseudoCounter in flyscan
       - [ ] Zibi will at least comment of what exactly is needed

5. Review of the oldest 15 issues in the backlog
   - we agree on using *stale* label for labeling issues that we close due to lack of resources
   - we should be careful when closing bugs
   - we managed to review 4 issues - close one, ping one reporter and design one

6. AOB
   - 12-01-2023 - organized by DESY (Teresa)
