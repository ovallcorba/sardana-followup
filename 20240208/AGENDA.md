# Agenda for the Sardana Follow-up Meeting - 2024/02/08

Held on Thursday 2024/02/08 at 14:00

Organizer: Teresa @ DESY

## Agenda

1. Sardana Redis recorder using blissdata 1.0.
   - News about meeting in March: preparation needed, date fixed (should a
     doodle be created) ?

2. Sardana configuration improvements (SEP20).
   - Status of MR https://gitlab.com/sardana-org/sardana/-/merge_requests/1968
   - anything else?

3. Operation highlights, user's feedback, issues, etc. - Round table
   - ALBA
   - DESY
   - MAXIV
   - MBI Berlin
   - SOLARIS

4. Review pending points from the previous meetings

   - From last meeting
   - From previous meetings:
     - [ ] Sardana roadmap: set milestone for the next release and assign short terms issues to it + add assignees
       - Zibi wanted to take care of that
     - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab (no update x2)

5. Jan24 release status
   - issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues
   - MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests

6. Sardana Users Training

7. Overview of current developments / MR distribution

8. Review of the oldest 15 issues in the backlog

9. AOB
   - Next meeting?
