# Minutes for the Sardana Follow-up Meeting - 2024/03/14

Held on Thursday 2024/03/14 at 14:00

Organizer: Michal @ SOLARIS

Participants: Zibi, Oriol, Jordi, Michal, Teresa, Vanessa

## Minutes

1. Jan24 release status
   - issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues
     - no more issues left for the milestone
   - MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests
     - some MR left:
       - [hotfix 3.4.4](https://gitlab.com/sardana-org/sardana/-/merge_requests/1974) merging as a hotfix release
       - still 3 MR WIP but we distributed work of review and merging, should be finished soon
       - we are monitoring status, if the work takes too much time, we will eventually exclude some MR from the release
   - ALBA and SOLARIS are release managers
     - we will meet and distribute [work](https://gitlab.com/sardana-org/sardana/-/wikis/How-to-release-Sardana)
   - platforms for manual tests:
     - Centos7 - SOLARIS
     - Debian 11 and 12 - DESY
     - Debian 10 - ALBA
     - conda (with the latest conda-forge dependencies) - MAX IV

2. Operation highlights, user's feedback, issues, etc. - Round table
   - ALBA
     - migrated last beamline to use non numeric ids, in the next shutdown will migrate one Pool on the machine
     - in the next release will start using config tool
   - DESY
     - [pickle protocol property #1933](https://gitlab.com/sardana-org/sardana/-/issues/1933) already solved
     - no other issues
   - MAXIV
     - rscan, fscan to be deterministic [#1941](https://gitlab.com/sardana-org/sardana/-/issues/1941)
     - problems with state handling the machine (must be done in software) [#1935](https://gitlab.com/sardana-org/sardana/-/issues/1935)
       - state based on frames acquired is current solution
     - meshct with parametric trajectories in IcePAP
       - first PoC implementation done
       - plans for meshctp (with parameter) for multiple meshes
   - MBI Berlin
     - not present
   - SOLARIS
     - tango serialization monitor acquiring timeout 
       - custom motor controller reading/setting attributes of its own motor via device proxy in a callback method
       - there are corner cases where locks on a tango layer happen 
       - to study more and start an issue to explain in detail and start discussion
       - MP will prepare a recipe to reproduce this on a dummy elements
     - pyqtgraph issue [#1936](https://gitlab.com/sardana-org/sardana/-/issues/1936)
       - to be tested with other versions (`0.11.1`)
     - some ideas of `expstatus` and `expconf` widgets improvements coming from the beamlines
       - there are issues opened, should be disussed there
       - [#1927](https://gitlab.com/sardana-org/sardana/-/issues/1927) for `expstatus`

3. Review pending points from the previous meetings

   - From last meeting
     - Sardana Redis recorder using blissdata 1.0.
       - [ ] Zibi will reply to ESRF with the status and a conclusion of
         the tests in ALBA.We will then propose a date for the next meeting.
         - email sent
         - the meeting is being planned
     - Sardana Users Training
       - [ ] It is planned to have a meeting after we define what we will plan.
         Solaris and MaxIV will prepare requirements. 
         Michal coordinator. We will call for a dedicated meeting.
         - all the institutes (MAX IV, ALBA, SOLARIS) should gather their internal feedback from the users of their needs
         - later we can share the results and see what are the common points
   - From previous meetings:
     - [x] Scan GUI: MAX IV will make it visible on their public Gitlab (no update x3)
       - it is visible now [here](https://gitlab.com/MaxIV/app-maxiv-scangui3)
       - readme will be updated soon

4. Overview of current developments / MR distribution
   - Sardana configuration improvements (SEP20). - status update
     - skipped
   - MRs related to showscan [!1983](https://gitlab.com/sardana-org/sardana/-/merge_requests/1983) and 
   [!1982](https://gitlab.com/sardana-org/sardana/-/merge_requests/1982) wait for the answers
   - Improve Timescan macro plotting in showscan online [!1980](https://gitlab.com/sardana-org/sardana/-/merge_requests/1980) 
     - Vanessa will review it
   - reviewers that cannot merge themselves, when the MR is ready, should ping integrators to do the merge (pinging sardana-org)
   - top priority developments from ALBA point of view (roadmap from Sardana Continous Scans Workshop)
     - Allow different synchronization descriptions for controlling the shutter, one shutter opening for the whole sequence or one shutter opening for each acquisition 
     - Diffractometer Sample-scan in BL06 - XAIRA: need to synchronize fast shutter based on constant velocity event 
     - Allow different synchronization descriptions for controlling any equipment e.g. shutter, detector, sample environment . Aim for API compatible with TFG, PanDABox and NI
     - this probably will require:
       - changes in the core
       - changes in the GSF (waypoint generator, improving docs, providing progamatic examples)
       - enancing expconf widget 
   - another important development:
     - improvements in meshct scans developed by MAX IV
   - shutter control by PandABox in Sardana [MAX IV example](https://gitlab.com/MaxIV/sardana-kitslabpandabox#synchronization-on-time-domain)
   
5. Review of the oldest 15 issues in the backlog
   - skipped

6. AOB
   - moving sardana-icepap project to sardana-org on gitlab (proposed during the sardana icepap meeting)
     - create an issue to discuss this 
     - maybe move sardana-tango in the future as well
   - sardana followups advertised on [tango indico](https://indico.tango-controls.org/)? (similar to taurus followups) 
     - good idea, but we need someone to grant us permissions to create events there
     - maybe add `info@tango-controls.org` mailing group (similar to taurus followups) to email invitation
   - 38th Tango Meeting in May, start thinking about our contribution, monitor indico page to send abstract
     - Jan24 release - if ready
     - redis recorder
     - multiple files config
     - Continuous Scans Workshop in SOLARIS
     - shutter design controller - if ready
     - major bugfixes summary
     - interactions with tango ecosystem
   - continuous scans improvements developments dedicated meeting 
     - there will be a poll to select a date
   - Next meeting?
     - 18.04.2024 at 14:00, organized by ALBA