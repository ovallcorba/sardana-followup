# Agenda for the Sardana Follow-up Meeting - 2024/03/14

Held on Thursday 2024/03/14 at 14:00

Organizer: Michal @ SOLARIS

## Agenda

1. Jan24 release status
   - issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues
   - MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests

2. Operation highlights, user's feedback, issues, etc. - Round table
   - ALBA
   - DESY
   - MAXIV
   - MBI Berlin
   - SOLARIS
   - ...

3. Review pending points from the previous meetings

   - From last meeting
     - Sardana Redis recorder using blissdata 1.0.
       - [ ] Zibi will reply to ESRF with the status and a conclusion of
         the tests in ALBA.We will then propose a date for the next meeting.
     - Sardana Users Training
       - [ ] It is planned to have a meeting after we define what we will plan.
         Solaris and MaxIV will prepare requirements. 
         Michal coordinator. We will call for a dedicated meeting.
   - From previous meetings:
     - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab (no update x3)

4. Overview of current developments / MR distribution
    - Sardana configuration improvements (SEP20). - status update

5. Review of the oldest 15 issues in the backlog

6. AOB
   - Next meeting?
