# Minutes from the Sardana Follow-up Meeting - 2023/11/29

Held on Thursday 2023/11/29 at 14:00

Organizer: Zibi @ ALBA

Participants: Johan, Michael, Teresa, Jan, Oriol, Jordi, Marti, Zibi

## Minutes

1. Sardana Redis recorder using blissdata 1.0.
   - Respository: https://github.com/ALBA-Synchrotron/sardana-redis
   - Follow-up meeting with the blissdata, flint and NexusWriter developers from the ESRF:
     Monday 11/12 14:00-16:00.
   - We briefly commented the idea behind the recorder to be able to collaborate
     with the Bliss project on all the downstream tools after publishing data in Redis.

2. Sardana configuration improvements (SEP20).
   - bug fixes
     - Johan addressed most of them. Two remaining #1905 and #1917 will be addressed by Marti.
   - configuration spread in multiple files
     - Johan is very close to make it working. This will be included in the release.
   - graph
     - Very nice tool. We already discussed it at ALBA and feedback was provided to Johan.
     - It will also be included in the release.

3. Operation highlights, user's feedback, issues, etc. - Round table
   - ALBA
     - migrating beamlines to non-numerical ID. 
       In this process finding and fixing issues in the migration script.
   - DESY
     - Found an issue in the SPEC file recorder in the pre-scan snapshot 
       - reported as https://gitlab.com/sardana-org/sardana/-/issues/1922
   - MAXIV
     - Found an issue that the Pool elements in the MacroServer disappear after the Pool restart.
       - This is a very old issue and has to do with the cache of elements in the MacroServer
       - https://gitlab.com/sardana-org/sardana/-/issues/113
   - MBI Berlin
     - Upgrading to Sardana 3.4
     - Found issues with the fact that the environment is now passed as a copy and not as a reference
     - Currently have 6 Sardana setups
       - 1 setup (Mobile Synchrotron Setup) is trying to use the YAML configuration
   - SOLARIS (excused)

4. Review pending points from the previous meetings

   - From last meeting
     - [ ] Sardana roadmap: set milestone for the next release and assign short terms issues to it + add assignees
     - [ ] missing aliases of elements (Pool, MacroServer, Door...) crashes configuration dump - Michal will create an issue
   - From previous meetings:
     - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab (no update x1)

5. Jul23 release status
   - rename milestone?
     - yes. Milestone was renamed to Jan24.
   - select MR/issues for the milestone
     - We mostly selected issues/MR related with configuration format and tools
     - issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues
     - MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests
   - do we need 3.4.4 hotfix (DESY use latest release with cherry picking from develop)
     - no, there is no need. DESY continue using their own branch.

6. Overview of current developments / MR distribution
   - Showscan online improvements
     - Commented on the possible implementation: https://gitlab.com/sardana-org/sardana/-/issues/1913
     - Flint could be an alternative, but we don't know when it could be available (also it would force to use Redis)

7. Review of the oldest 15 issues in the backlog
   - Will be probably skipped due the lack of time

8. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.
     - Needs discussion of pros and cons.
   - Next meeting: 11/01/2024
