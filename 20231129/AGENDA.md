# Agenda for the Sardana Follow-up Meeting - 2023/11/29

Held on Thursday 2023/11/29 at 14:00

Organizer: Zibi @ ALBA

## Agenda

1. Sardana Redis recorder using blissdata 1.0.
   - Respository: https://github.com/ALBA-Synchrotron/sardana-redis
   - Follow-up meeting with the blissdata, flint and NexusWriter developers from the ESRF:
     Monday 11/12 14:00-16:00.

2. Sardana configuration improvements (SEP20).
   - bug fixes
   - configuration spread in multiple files
   - graph
   - ...

3. Operation highlights, user's feedback, issues, etc. - Round table
   - ALBA
   - DESY
   - MAXIV
   - MBI Berlin
   - SOLARIS

4. Review pending points from the previous meetings

   - From last meeting
     - [ ] Sardana roadmap: set milestone for the next release and assign short terms issues to it + add assignees
     - [ ] missing aliases of elements (Pool, MacroServer, Door...) crashes configuration dump - Michal will create an issue
   - From previous meetings:
     - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab (no update x1)

5. Jul23 release status
   - rename milestone?
   - select MR/issues for the milestone
   - do we need 3.4.4 hotfix (DESY use latest release with cherry picking from develop)

6. Overview of current developments / MR distribution
   - Showscan online improvements      - ...

7. Review of the oldest 15 issues in the backlog
   - Will be probably skipped due the lack of time

8. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.
     - Needs discussion of pros and cons.
   - Next meeting?
