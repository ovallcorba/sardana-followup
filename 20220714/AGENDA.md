# Agenda for the Sardana Follow-up Meeting - 2022/07/14
To be held on Thursday 2022/07/14 at 14:00

## Agenda

1. Brief review sardana-jupyter
  
2. Sardana release Jul22
   
   - still pending issues to be included ?
   
3. Sardana Configuration Tool - see [!1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
   
   - current status and how to proceed
  
4. Urgent user problems/issues - Round table
   
    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - MBI
    - ...

5. Review pending points from the previous meetings

   - From last meeting:
        
   - From previous meetings:
     - Discussion on limit switches
       - [ ] issue raised about macro names for setting limits unification [#1641](https://gitlab.com/sardana-org/sardana/-/issues/1641) -  Daniel will propose a MR for the naming
     - [ ] Reported by MAXIV: Issue with extra axis attributes on pseudo motor not causing change events on position
           https://gitlab.com/sardana-org/sardana/issues/1693 <- related
           - no update x 1
     - [ ] Issue with configurationn of dynamic attributes, mostly on the tango layer. Henry is working on that: sending
           a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
           Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
           example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.
           - no update x 2
          
6. Overview of current developments / MR distribution
   - 1D channels in SPEC header (storage files) [!1746](https://gitlab.com/sardana-org/sardana/-/merge_requests/1746)
   - Allow to skip last point in scans and change timescan first parameter to nb_points (previously nr_interv)
[!1634](https://gitlab.com/sardana-org/sardana/-/merge_requests/1634)
   - fix: avoid TangoMonitor timeout on pushing MeasurementGroup State event [!1772](https://gitlab.com/sardana-org/sardana/-/merge_requests/1772)
   - What to return when pseudo calculation fails: NaN or exception [#1749](https://gitlab.com/sardana-org/sardana/-/issues/1749)
   - scanstats for 2d/3d scans [#1748](https://gitlab.com/sardana-org/sardana/-/issues/1748)

7. Discussion about Bug Squashing Party #2   

8. AOB
   - Next meeting?
