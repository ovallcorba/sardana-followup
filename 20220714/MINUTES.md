# Minutes Sardana Followup - 2022/07/14
Held on Thursday 2022/07/14 at 14:00

Present:
- Zibi (ALBA)
- Michal (SOLARIS)
- Johan, Emil (MaxIV)
- Teresa (DESY)

## Agenda

1. Brief review sardana-jupyter

   Explanation from Zibi about the project:
   Macroserver runs in jupyter kernel, no as tango server, Pool runs
   as tango server. Pool elements are exposed and hence accesible
   via notebooks.
   Refactoring needed for improvements in plots, debugging ...
   Macros can run not only in notebook cells but also in a
   console analog to spock.
   Macroserver does not have properties (like running as tango
   server) but uses a file for the configuration. The MS environment
   works like before.

   Johan and Teresa will try it.
 
2. Sardana release Jul22
   
   - still pending issues to be included:
     !1772 -> it would be merged this week 
     !1664 -> it is shifted to the next milestone

   - A problem came yesterday: sardana fails with the lastest PyTango,
     the problem has to be fixed in PyTango. If it is not
     done soon, the latest PyTango version will be excluded from this release.

   The release will start next week (done by Johan and Teresa)

3. Sardana Configuration Tool - see [!1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
   
   - current status and how to proceed

   Johan presented the file (.yaml) he proposes as a file format for the configuration. There was a long discussion
   about it, it looks nice and complete.
   Feedback from us and possibly institutes will be given in a dedicated meeting (probably in September)
   The file will be put in the SEP20 branch (or dedicated repo) to collect comments.
  
4. Urgent user problems/issues - Round table
  
    - SOLARIS
      Nothing
    - MAXIV
      Migrating beamlines to python3 and sardana3.
    - DESY
      Nothing
    - ALBA
      Upgrading to python3 the last 2 beamlines and to debian10.
      Moving motors to wrong positions was fixed by Tango.
      Timeout issues also solved.

5. Review pending points from the previous meetings

   - From last meeting:
        
   - From previous meetings:
     - Discussion on limit switches
       - [ ] issue raised about macro names for setting limits unification [#1641](https://gitlab.com/sardana-org/sardana/-/issues/1641) -  Daniel will propose a MR for the naming
           - no update x 1
     - [ ] Reported by MAXIV: Issue with extra axis attributes on pseudo motor not causing change events on position
           https://gitlab.com/sardana-org/sardana/issues/1693 <- related
           - no update x 2
     - [ ] Issue with configurationn of dynamic attributes, mostly on the tango layer. Henry is working on that: sending
           a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
           Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
           example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.

           There is a MR (!1677), it will be reviewed by Zibi.

6. Overview of current developments / MR distribution
   - 1D channels in SPEC header (storage files) [!1746](https://gitlab.com/sardana-org/sardana/-/merge_requests/1746)

     Merged

   - Allow to skip last point in scans and change timescan first parameter to nb_points (previously nr_interv)
!1634

     We will use negative values for skipping last point and not use 0. It will be also done for step scans.
     Zibi will review it and merge it.
     

   - fix: avoid TangoMonitor timeout on pushing MeasurementGroup State event !1772

     Discussed and it will be merged.
     
   - What to return when pseudo calculation fails: NaN or exception #1749

     Discussed, an specific exception will be send. Zibi will work on that. Some last
     comments will be added to the issue.

   - scanstats for 2d/3d scans #1748

     Not time to be discussed.

7. Discussion about Bug Squashing Party #2

     Not time to be discussed.


8. AOB

    - Next meeting will be held on September, the 1st. Organized by Johan.
