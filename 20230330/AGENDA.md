# Agenda for the Sardana Follow-up Meeting - 2023/03/30

To be held on Thursday 2023/03/30 at 14:00

## Agenda

1. Jan23 Release
    - status report
   
2. Operation highlights, user's feedback, issues, etc. - Round table
   
    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - MBI
    - ...

3. Review pending points from the previous meetings

  - From last meeting:
    - Problems with aborting macros with Ctrl-C, seemingly caused by Taurus GUIs subscribing to elements. At least, the problems went away when some GUI was killed. The GUI was in particular polling the Door state.
        Potentially related issue https://gitlab.com/sardana-org/sardana/-/issues/1431
        Sardana logs provided by Teresa.
    - Problem with hangs on moves were reproduced on one BL, running with Tango 7 (!). This was related to GC in taurus, leading to a deadlock. Solution may be to replace Taurus with a normal proxy, or just upgrade Tango. Further investigation needed.
  - From previous meetings:
    - [ ] Zibi will provide diagrams on what controllers should return in `ReadOne()` - requested by MAXIV
      - No news yet x1
    - [ ] Wojciech will answer the comments in th MR (Pool should not log an ERROR if hkl is not installed - #394)
      - He promised to look at this MR tomorrow.
    - PseudoCounter in flyscan
      - [ ] Zibi will at least comment of what exactly is needed
       - No update x1
	
     
4. Overview of current developments / MR distribution
   - Review project board

5. Sardana workshop in SOLARIS
   - feedback and short discussion

6. Review of the oldest 15 issues in the backlog

7. AOB
   - Next meeting?

