# Minutes for the Sardana Follow-up Meeting - 2022/10/13

Held on Thursday 2022/10/13 at 14:00

Present:
- Zibi (ALBA)
- Michal (SOLARIS) chair
- Teresa (DESY)
- Johan (MAXIV)
- Daniel (MBI)
- et. al.

## Agenda

1. Sardana Tutorial prior Bug Squashing Party #2
   
   - overview of Sardana core and preparation of environment by Zibi
     - TODO: link
   - overview of pool core tests and controller plugin tests by Michal
     - docs: [from the development branch: » Developer’s Guide » Writing controllers » How to test controller plugins](https://sardana-org.gitlab.io/-/sardana/-/jobs/3156747504/artifacts/public/devel/howto_controllers/howto_controller_testing.html)
  
2. Urgent user problems/issues - Round table
   
    - SOLARIS
      - one minor: Can't change number format of PoolMotorTV widget - [#1778](https://gitlab.com/sardana-org/sardana/-/issues/1778), already fixed
    - MAXIV
      - no issues
    - DESY
      - no issues
    - ALBA
      - spock json encoding warnings after running `expconf` - more investigation needed, no issue created yet
    - MBI
      - old one: PseudoCounters based on another PseudoCounters - now it is possible! - [#823](https://gitlab.com/sardana-org/sardana/-/issues/823) is now closed
      - duplicate alias on same tango hosts issue is creating a lot of additional efforts - [#988](https://gitlab.com/sardana-org/sardana/-/issues/998) 
        - fundamental design issue, for now stay with the proposed PoC 

3. Review pending points from the previous meetings

   - From last meeting:
     - [x] Again facing problems with taurus unsubscribing in attribute's `__del__()` - https://gitlab.com/tango-controls/pytango/-/issues/413. Found a bug in PyTango/cppTango - to be reported. Can be reproduced with `test_dev_gc` macro - https://gitlab.com/taurus-org/taurus/-/merge_requests/1093#note_531346632. We should add this macro to the testsuite.
       - bug was in Sardana not using OmniThreads, already fixed in [#1791](https://gitlab.com/sardana-org/sardana/-/merge_requests/1791)
     - [ ] Hangs on Pool shutdown if there are event subscriptions present
       - no update x1, no issue yet
     - [x] expconf wrongly modify old PreScanSnapshot names - [#1771](https://gitlab.com/sardana-org/sardana/-/issues/1771). Probably could be closed as *wontfix*.
       - already closed
          
4. Overview of current developments / MR distribution
   - Decide on property name of GScan object [#784](https://gitlab.com/sardana-org/sardana/-/issues/784)
     - decided on `gscan`!
   - SEP20 works [#1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749), [#1802](https://gitlab.com/sardana-org/sardana/-/merge_requests/1802)
   - scanstats for Nd-scans with multiple motors [#1789](https://gitlab.com/sardana-org/sardana/-/merge_requests/1789)
   - add error catching for ending records [#1770](https://gitlab.com/sardana-org/sardana/-/merge_requests/1770) 
   - preparations for SBSP #2

5. AOB
   - Book 15min slot during the next meeting to review and close the oldest issues
   - Requested for an Open Source project status for Sardana on Gitlab
   - Next meeting? 
     - November 17th at 14:00 organized by ALBA (Zibi)
