# Agenda for the Sardana Follow-up Meeting - 2022/10/13

To be held on Thursday 2022/10/13 at 14:00

## Agenda

1. Sardana Tutorial prior Bug Squashing Party #2
   
   - overview of Sardana core and preparation of environment by Zibi
   - overview of pool core tests and controller plugin tests by Michal
  
2. Urgent user problems/issues - Round table
   
    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - MBI
    - ...

3. Review pending points from the previous meetings

   - From last meeting:
     - [ ] Again facing problems with taurus unsubscribing in attribute's `__del__()` - https://gitlab.com/tango-controls/pytango/-/issues/413. Found a bug in PyTango/cppTango - to be reported. Can be reproduced with `test_dev_gc` macro - https://gitlab.com/taurus-org/taurus/-/merge_requests/1093#note_531346632. We should add this macro to the testsuite.
     - [ ] Hangs on Pool shutdown if there are event subscriptions present
     - [ ] expconf wrongly modify old PreScanSnapshot names - [#1771](https://gitlab.com/sardana-org/sardana/-/issues/1771). Probably could be closed as *wontfix*.
          
4. Overview of current developments / MR distribution
   - Decide on property name of GScan object [#784](https://gitlab.com/sardana-org/sardana/-/issues/784)
   - ?

5. AOB
   - Next meeting?
