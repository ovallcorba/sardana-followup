# Minutes from the Sardana Follow-up Meeting - 2023/07/06

Held on Thursday 2023/07/06 at 14:00

Organizer: Teresa @ DESY

Participants:

Zibi (ALBA)
Johan (MAXIV)
Michal (Solaris)
Teresa (DESY)
Michael Schneider (MBI)

## Agenda

Short introduction due to the presence of Michael as new participant,
he will take part of these meetings as replacement of Daniel Schick.


1. Discussion about Jul23 release
    - target it as Jul23, delay, skip ... ?

  There are some fixes:
  
   https://gitlab.com/sardana-org/sardana/-/blob/develop/CHANGELOG.md

   but not new features for the moment.
   A hot fix will be done as soon as possible with those fixes. MRs can be marked to be added to
   this hot fix and it will be evaluated if it is easy to add them.
   A hot fix related to Taurus will also be added.
   The continuous scan fix (MR 1908) has been marked during the meeting.
   A new release will be done as soon as the experiment status widget:
   https://gitlab.com/sardana-org/sardana/-/issues/1489
   is ready. Perhaps the feature of multiple files in the SEP20 could be also ready at
   that point and also included in the release.
   A new issue for coordinating the hot fix releases will be created by Zibi after
   this meeting.
   
2. SEP20 status    
    - is it possible to use `sardana config dump` output as `sardana config diff` input
      - [x] Zibi will check with `-` as argument
      It works.
    - multiple config files - see https://gitlab.com/sardana-org/sardana/-/issues/1841
      The issue is updated some days ago by Johan with some proposal.
      Michael will have a look at it.
      Discussion about the output in case of multiple files: it will not be able to redirect
      the output of one update to a file. Some ideas are there, it will be discussed.
      Zibi will add some comments with new ideas.
      About the way of accesing the environment variables, Zibi made some comments. He asked
      if it would be feasible to use Tango attributes for that. A same environment variable
      can have different levels (Macroserver - global one, Door, macros, ...), this was unknown
      for Johan. Zibi will document it. The environment is important for example for GUIs
      or for the SEP20. What Zibi propose is to use memorized attributes, but the different
      levels of the environment variables could be a problem for that. This need design and
      discussion, a new issue could be a good starting point. Zibi will in any case start with
      the documentation and from there one can see how is better to proceed.
      
          
3. Operation highlights, user's feedback, issues, etc. - Round table
   
    - ALBA:
      - problems with MG and disable channels where the controllers are offline. There is an
        internal issue in Alba. It is often happening in ALBA. An sardana issue will be created.
      - there are some inestabilties in one beamline, it could be due to controllers or to sardana.
      - One beamline updated to 3.2 for not using numerical ids. No problems. They make a config dump in one
        beamline but Zibi has not checked the output. They will start using config tool there.
      - deploy sardana using conda in one beamline. They will make some tests and if it is fine
        perhaps they will migrate the other beamlines and rely on that. IT will be easy for
	maintenance of python versions, Zibi want to discuss which python versions should be
	maintained, he will create an issue for discussion.
    - DESY
      - not issues
    - MAXIV
      - summer shutdown since one week for 6 weeks. They will try to update to sardana 3.4 some
        more beamlines. They have not updated the numerical ids in any of them.
      - After update to 3.4:
        - they found a change in the continuous scans generator and broke a continuous scan macro.
	  They have not found anything in the log of the 3.4 about changes related to this.
	  It is not a problem, they solved it but it was unexpected.
        - also problems with motor groups, some motors were hanging. Johan will give more details about
          it. It could be caused by a problem with a motor controller. Zibi thinks that is usually
	  due to problems with pytango and tango, for example mixing versions. But it does not seem
	  to be the case.
        - the registers have not events now, Johan will look also for more details about it.
      - They change the way they store the local logs. Now they use graylog, it is open source.
        Johan offer some configuration files to handle it. fluentbit is what they use instead of filebeat 
    - MBI Berlin
      - Michael is starting working on sardana. He has found some issues in the config tool, but he will
        check further before making an issue. Zibi pointed out an issue about working around the alias
	limitation in tango.
    - SOLARIS
       - starting shutdown, nothing serious.
       - he asked if there is something specific in the read or state method of a controller when making
         continuous scan with hardware trigger. Zibi said that the state should be always MOVING during the
	 acquisition. For the read method it is documented what has to be returned it was recently done
	 and documented:
	 https://sardana-controls.org/devel/howto_controllers/howto_countertimercontroller.html#get-counter-values
    
4. Review pending points from the previous meetings

  - From last meeting
       - [x] Improve handling of CTScan (continuous scan) motion errors. MR will be reviewed by Johan
         It is merged.
       - [x] search on the sardana web page does not work - issue to be created
         It was fixed.
    - From previous meetings:
       - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab
         Still not done.
       - [Experiment status widget](https://gitlab.com/sardana-org/sardana/-/issues/1489):
          - [ ] Michal will make a MR for the `wa` macro with regard to reserved elements
	  Not done
       - [x] Jordi working on diagrams on what controllers should return in `ReadOne()` - requested by MAXIV
          The documentations was updated, in several chapters.
     
5. Overview of current developments / MR distribution
   - Review project board
      - type hints - see https://gitlab.com/sardana-org/sardana/-/merge_requests/1915
        Johan introduced this topic, he saw duplication warnings in docstrings.
	The idea was replace it with python type hints. It seems to work well and
	he does not see drawbacks. It solves the duplication warnings.
	It could be complicated to implement it in sardana.
	Michael has some experience, but he also thinks that it could be complicated
	if the types are not standard.
	It will be interesting to try but it could be a big amount of work.
	We agree on going for this. We merge Johan MR as an starting point.
	
      - docstring style - see https://gitlab.com/sardana-org/sardana/-/issues/1535
        We also agree for that. It does not change the existing documentation.
	Johan said that it could be a little bit inconsistent, but if problems appear
	we will look at that.
	
      - mAPI and not mAPI methods - see https://gitlab.com/sardana-org/sardana/-/merge_requests/1917
        Postpone it. We can comment in the issue.

6. Sardana workshop in SOLARIS
   - current status of organization
     Date fixed, indico ready, advertised by e-mail, tango meeting and speakers were
     contacted. Perhaps put it in the tango website in the news.
     Zibi proposed to create working groups with people working on those topics and prepare
     some slides about how it was implemented in the different sides. Collaboration between
     the different solutions and a roadmap about how to continue in sardana.
     There are four challenge features: trajectories, very fast scans, multiple capability controllers and
     more complex synchronitazion description. We agree in creating groups about these topics.
     Zibi will send an e-mail with the ideas.
     There are some smaller features that will be easier to prepare and probably they do not need
     some previous work.

7. Sardana contributions to events and conferences
   - Tango Workshop at ICALEPCS2023
     - Any preparation needed?
   - ICALEPCS2023
     - Overleaf was proposed by Michal as common edit tool, final decission?
     - Official template already available ?

     Skip it for lack of time.

8. Review of the oldest 15 issues in the backlog

   Skip it for lack of time.
9. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.

     Skip it for lack of time.
     
   - Next meeting?. Organizer: Johan @ MAXIV

     17th August at 14:00
