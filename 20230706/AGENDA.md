# Agenda for the Sardana Follow-up Meeting - 2023/07/06

To be held on Thursday 2023/07/06 at 14:00

Organizer: Teresa @ DESY

## Agenda

1. Discussion about Jul23 release
    - target it as Jul23, delay, skip ... ?

2. SEP20 status    
    - is it possible to use `sardana config dump` output as `sardana config diff` input
      - [ ] Zibi will check with `-` as argument
    - multiple config files - see https://gitlab.com/sardana-org/sardana/-/issues/1841
          
3. Operation highlights, user's feedback, issues, etc. - Round table
   
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS
    
4. Review pending points from the previous meetings

  - From last meeting
       - [ ] Improve handling of CTScan (continuous scan) motion errors MR will be reviewed by Johan
       - [ ] search on the sardana web page does not work - issue to be created
       
    - From previous meetings:
       - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab
       - [Experiment status widget](https://gitlab.com/sardana-org/sardana/-/issues/1489):
          - [ ] Michal will make a MR for the `wa` macro with regard to reserved elements
       - [ ] Jordi working on diagrams on what controllers should return in `ReadOne()` - requested by MAXIV
     
5. Overview of current developments / MR distribution
   - Review project board
      - type hints - see https://gitlab.com/sardana-org/sardana/-/merge_requests/1915
      - docstring style - see https://gitlab.com/sardana-org/sardana/-/issues/1535
      - mAPI and not mAPI methods - see https://gitlab.com/sardana-org/sardana/-/merge_requests/1917

6. Sardana workshop in SOLARIS
   - current status of organization

7. Sardana contributions to events and conferences
   - Tango Workshop at ICALEPCS2023
     - Any preparation needed?
   - ICALEPCS2023
     - Overleaf was proposed by Michal as common edit tool, final decission?
     - Official template already available ?

8. Review of the oldest 15 issues in the backlog

9. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.
   - Next meeting?. Organizer: Johan @ MAXIV
