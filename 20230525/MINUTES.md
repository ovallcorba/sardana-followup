# Minutes from the Sardana Follow-up Meeting - 2023/05/25

Held on Thursday 2023/05/25 at 14:00

Organizer: Zibi @ ALBA
Participants: Johan (only first 30 min), Michal, Teresa, Zibi 

## Agenda

1. Jan23 Release
    - release summary
      - release was delayed more than 2 months
      - no Windows tests were performed during the release process
        - due to this we discovered late an issue with the `sardana` script name conflict
        - workaround was applied for Windows - `sardanacli` script
        - a request was raised to Tango starter [Start device servers with custom scripts](https://gitlab.com/tango-controls/starter/-/issues/20)
      - conda and pip packages were split - https://gitlab.com/sardana-org/sardana/-/issues/1493
      - no issues after the release but: [taurus dependency issue]( https://gitlab.com/sardana-org/sardana/-/issues/1859) - discovered at MAXIV
    - deployment status, SEP20 status?
      - At MAXIV one testing beamline tried to use 3.4 but found issues after upgrading taurus, with taurus ini files
        - As an extra point we discussed how taurus uses ini settings and how to export dynamic panels from ini files to xml
      - Two beamlines at ALBA use 3.4 already, but only one with SEP20 and after migration to non-numerical ids. So far no problems.
      - SOLARIS will maybe upgrade during summer shutdown
      - DESY don't plan to upgrade so far
      - Regarding SEP20: is it possible to use `sardana config dump` output as `sardana config diff` input
        - [ ] Zibi will check with `-` as argument
    - next release
      - We will decide on the next follow-up meeting if we target Jul23 for July, delay or skip.
      - Managers: ALBA & SOLARIS
   
2. Operation highlights, user's feedback, issues, etc. - Round table
   
    - ALBA
      - Considering to update to Tango 9.4 to solve motion hangs
        - Taurus Performance Optimization project showed some segfaults when stress testing Tango 9.4 - this may negatively affect the eventual upgrade
    - DESY
      - Nothing to highlight
    - MAXIV
      - started a project about reliability of experiments
        - MAXIV + Zibi are on a group chat on Slack for the organization matters, but technical discussions will be documented or moved to GitLab as early as possible 
        - we already discovered one bug in continuous scans about motion error message - see point 4 for more details
    - MBI Berlin
      - Not present
    - SOLARIS
      - Nothing to highlight
    
3. Review pending points from the previous meetings

  - From last meeting:
    - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab
      - no update, Johan had to leave earlier
    - [x] Highlighted from ALBA: MacroServer DB file corrupted (already happened twice). Update?
      - In case of ALBA, moving from NFS to local FS helped at one beamline.
      - MBI suffers the same issues, but they are already on the local FS - we will continue discussing possible improvements on [#1824](https://gitlab.com/sardana-org/sardana/-/issues/1824)
    - [x] Problem with hangs on moves were reproduced on one BL, running with Tango 7 (!). This was related to GC in taurus, leading to a deadlock. Solution may be to replace  Taurus with a normal proxy, or just upgrade Tango. Further investigation needed. No issue created yet.
      - Two issues were created and will be kept in the "Known Problems" section of the docs:
        - [#1856](https://gitlab.com/sardana-org/sardana/-/issues/1856)
        - [#1857](https://gitlab.com/sardana-org/sardana/-/issues/1857)
  - From previous meetings:
    - [x] Jordi working on diagrams on what controllers should return in `ReadOne()` - requested by MAXIV
      - See newly added documentation in:
        - https://sardana-controls.org/devel/howto_controllers/howto_countertimercontroller.html#get-counter-value
        - https://sardana-controls.org/devel/howto_controllers/howto_countertimercontroller.html#get-counter-values
    - [ ] PseudoCounter in flyscan - Zibi will at least comment of what exactly is needed
       - No update x3 - to be remove from pending points
     
4. Overview of current developments / MR distribution
   
   - Review project board
     - no really big movement since the last meeting
   - [Experiment status widget](https://gitlab.com/sardana-org/sardana/-/issues/1489)
     - Zibi explained the GUI:
       - area with the reserved elements organized in a tree view
       - area with the door state and status and commands
     - ReservedElements attribute was added to the door
     - [ ] Michal will make a MR for the `wa` macro with regard to reserved elements
   - Update channels' state during action loop https://gitlab.com/sardana-org/sardana/-/issues/1818
     - motion action works but the logic is very complicated and uses some hacks in the code
     - we will focus on fixing the acquisition action now
   - Improve handling of CTScan (continuous scan) motion errors MR will be reviewed by Johan
   - HKL change will be proposed by Teresa soon

5. Sardana workshop in SOLARIS
   
   - dates - see [poll](https://framadate.org/B61Wma97XZVZNw9j)
     - September winning so far
     - reminder will be sent tomorrow to invitations with no replies
   - content - see [Indico page](https://indico.solaris.edu.pl/event/5/)
     - after agreeing on date and speakers we will ask if clarifications are needed to the expected content

6. Sardana contributions to events and conferences
   
   - ICALEPCS2023
     - how to edit?
     - Michal proposed latex - he can host it on his Overleaf pro account (we will have review option and unlimited number of collaborators)
     - We wait for the official template
   - Tango Meeting - Zibi will give it
     - Sardana and Taurus status will include:
       - Configuration tool
       - Continuous scans workshop
       - Experiment status
       - TPO 
       - Maintenance: compatibility issues with recent python versions and dependencies
   - Tango Workshop at ICALEPCS2023
     - proposals: Taurus intro, scanning with Sardana and macro execution: doors, spock, macroexecutor, etc.

7. Review of the oldest 15 issues in the backlog
   
   - for the moment we will continue with the manual review but we discussed the automatic bot option today
   - reviewed just 2 issues including issue #66.

8. AOB
   
   - search on the web page does not work - issue to be created
   - Next meeting?
     - When: 22 of June
     - Organizer: Teresa @ DESY
