# Agenda for the Sardana Follow-up Meeting - 2023/05/25

To be held on Thursday 2023/05/25 at 14:00

Organizer: Zibi @ ALBA

## Agenda

1. Jan23 Release
    - release summary
    - deployment status, SEP20 status?
    - next release
   
2. Operation highlights, user's feedback, issues, etc. - Round table
   
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS
    

3. Review pending points from the previous meetings

  - From last meeting:
    - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab
    - [ ] Highlighted from ALBA: MacroServer DB file corrupted (already happened twice). Update?
    - [ ] Problem with hangs on moves were reproduced on one BL, running with Tango 7 (!). This was related to GC in taurus, leading to a deadlock. Solution may be to replace  Taurus with a normal proxy, or just upgrade Tango. Further investigation needed. No issue created yet.
  - From previous meetings:
    - [ ] Jordi working on diagrams on what controllers should return in `ReadOne()` - requested by MAXIV
    - [ ] PseudoCounter in flyscan - Zibi will at least comment of what exactly is needed
       - No update x2
     
4. Overview of current developments / MR distribution
   - Review project board
   - [Experiment status widget](https://gitlab.com/sardana-org/sardana/-/issues/1489)

5. Sardana workshop in SOLARIS
   - dates - see [poll](https://framadate.org/B61Wma97XZVZNw9j)
   - content - see [Indico page](https://indico.solaris.edu.pl/event/5/)

6. Sardana contributions to events and conferences
   - Tango Meeting
   - Tango Workshop at ICALEPCS2023
   - ICALEPCS2023

7. Review of the oldest 15 issues in the backlog
   - skipped

8. AOB
   - Next meeting? Organizer: Teresa @ DESY
