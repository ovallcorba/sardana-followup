# Agenda for the Sardana Follow-up Meeting - 2022/03/03

To be held on Thursday 2022/03/03 at 14:00

## Agenda

1. Urgent user problems/issues - Round table

    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
    - ...

2. Review pending points from the previous meetings

    - From last meeting:
    
        - [ ] ALBA: New requirement for continuous scans: configure the timeout for acquisition, now value fixed to 15 sec (after the movement, for
       avoiding scans hanging for ever). Roberto will create an issue and MR for that.
       
        - [ ] Issue with configurationn of dynamic attributes, mostly on the tango layer. Henry is working on that: sending
       a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
       Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
       example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.

        - [ ] MR approval rules?
    
    - From previous meetings:
        - [ ] Zibi will set up meeting about Sardana configuration tool (discussion about solutions and text formats)
        
        - [ ] `self.<macro name>` exceptions are uninformative.
            - looked into and agreed but on all levels of traceback it is uninformative 
            - issue in Sardana and description of the traceback loops needed, then discussion about how it should work
            
        - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
            - mentioned in round table
            
        - [ ] Problem with pseudocounters of pseudocounters. They work when first created, but not later (e.g. after restart).
	      There is some problem with the order in which they get created.
            - Daniel's question,  issue created, probably a bug
        - [ ] MacroServer startup hook (that would be called automatically after starting the server - an opposite to the at exit)
              - Zibi wanted to create a MR
        - [ ] Users can break Sardana by messing with configuration in Tango DB:
     	    - status of the diagnostics script:
                - [ ] Henrik wanted to register sherlock in the extras catalogue

        - [ ] For feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344):
            - ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)
	    
        - [ ] Simplify PseudoMotor configuration? [#1702](https://gitlab.com/sardana-org/sardana/-/issues/1702)
            - Henrik will work on a MR
    
3. Overview of current developments / MR distribution

    - Draft: Warn on conflict instead of remove and replace [!1677](https://gitlab.com/sardana-org/sardana/-/merge_requests/1677)

4. MR integration and approval rules

5. Sardana release ?
    
6. AOB
